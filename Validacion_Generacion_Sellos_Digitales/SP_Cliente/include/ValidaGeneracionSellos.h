#include <cxxtest/TestSuite.h>
#include <ClienteBD.h>
#include <string>
#include <stdlib.h>
#include <iostream>

class ValidaGeneracionSellos : public CxxTest::TestSuite {

    private:

        ClienteBD *bd = NULL;
        string nombre_prueba;

        int ejecutarConsulta(string consulta) {
            if(bd == NULL) {
                TS_FAIL("No se puede ejecutar la consulta porque el objeto bd es nulo");

                return 0;
            }

            if(!bd->estaConectado()) {
                TS_FAIL("No se puede ejecutar la consulta porque no hay conexión a la BD");

                return 0;
            }

            if(!bd->ejecutarProcedimientoAlmacenado(consulta)) {
                TS_FAIL("Error al ejecutar el procedimiento almacenado");

                return 0;
            }

            return atoi(bd->resultado().c_str());
        }

    public:

        void setUp() {
            nombre_prueba = nombre_prueba.empty() ? string("\n\nTEST: El regimen es nulo") : string("\n" + nombre_prueba);

            cout << nombre_prueba;

            bd = new ClienteBD();
            bd->conectar();
        }

        void tearDown() {
            bd->desconectar();
            bd = NULL;

            if(nombre_prueba == "Fin") {
                cout << "\n";
            }
        }

        /**
         * TEST: El régimen es nulo
         *
         * @param regimen       NULL
         * @param roles         SET{}
         * @param obligaciones  SET{}
         * @param actividades   SET{}
         *
         * @return 0 (false)
         */
        void test_regimen_nulo(void) {
            string consulta("sp_valida_gen_sd(NULL, 'SET{}', 'SET{}', 'SET{}')");

            TS_ASSERT_EQUALS(0, ejecutarConsulta(consulta));

            nombre_prueba = "TEST: El regimen no existe en la BD";
        }

        /**
         * TEST: El régimen no existe
         *
         * @param regimen       100
         * @param roles         SET{}
         * @param obligaciones  SET{}
         * @param actividades   SET{}
         *
         * @return 0 (false)
         */
        void test_regimen_no_existe(void) {
            string consulta("sp_valida_gen_sd(100, 'SET{}', 'SET{}', 'SET{}')");

            TS_ASSERT_EQUALS(0, ejecutarConsulta(consulta));

            nombre_prueba  = "TEST [Operador 1]: El regimen no tiene roles/obligaciones/actividades en la BD ni en el WS IDC";
        }

        /**
         * TEST: El régimen no tiene roles/obligaciones/actividades
         *
         * @param regimen       601
         * @param roles         SET{}
         * @param obligaciones  SET{}
         * @param actividades   SET{}
         *
         * @return 1 (true)
         */
        void test_op1_regimen_no_tiene_roles_obligs_activs_en_bd(void) {
            string consulta("sp_valida_gen_sd(601, 'SET{}', 'SET{}', 'SET{}')");

            TS_ASSERT_EQUALS(1, ejecutarConsulta(consulta));

            nombre_prueba  = "TEST [Operador 1]:  El regimen no tiene roles/obligaciones/actividades en la BD, pero en el WS IDC sí";            
        }

        /**
         * TEST: El régimen no tiene roles/obligaciones/actividades
         *
         * @param regimen       602
         * @param roles         SET{10,20,30}
         * @param obligaciones  SET{100,200}
         * @param actividades   SET{1000}
         *
         * @return 1 (true)
         */
        void test_op1_regimen_no_tiene_roles_obligs_activs_en_bd_pero_si_en_ws(void) {
            string consulta("sp_valida_gen_sd(602, 'SET{10,20,30}', 'SET{100,200}', 'SET{1000}')");

            TS_ASSERT_EQUALS(1, ejecutarConsulta(consulta));

            nombre_prueba  = "TEST [Operador 2]: El regimen tiene roles en la BD, pero en el WS IDC no";
        }

        /**
         * TEST: El régimen tiene roles
         *
         * @param regimen       606
         * @param roles         SET{}
         * @param obligaciones  SET{}
         * @param actividades   SET{}
         *
         * @return 0 (false)
         */
        void test_op2_regimen_tiene_roles_en_bd_pero_no_en_ws(void) {
            string consulta("sp_valida_gen_sd(606, 'SET{}', 'SET{}', 'SET{}')");

            TS_ASSERT_EQUALS(0, ejecutarConsulta(consulta));

            nombre_prueba  = "TEST [Operador 2]: El regimen tiene roles en la BD y en el WS IDC, pero no existe intersección";
        }

        /**
         * TEST: El régimen tiene roles
         *
         * @param regimen       606
         * @param roles         SET{400050,400055}
         * @param obligaciones  SET{}
         * @param actividades   SET{}
         *
         * @return 0 (false)
         */
        void test_op2_regimen_tiene_roles_en_bd_pero_en_ws_no_coinciden(void) {
            string consulta("sp_valida_gen_sd(606, 'SET{400050,400055}', 'SET{}', 'SET{}')");

            TS_ASSERT_EQUALS(0, ejecutarConsulta(consulta));

            nombre_prueba  = "TEST [Operador 2]: El regimen tiene roles en la BD y en el WS IDC, además de que se intersectan";
        }

        /**
         * TEST: El régimen tiene roles
         *
         * @param regimen       606
         * @param roles         SET{300033}
         * @param obligaciones  SET{}
         * @param actividades   SET{}
         *
         * @return 1 (true)
         */
        void test_op2_regimen_tiene_roles_en_bd_y_en_ws_coincide_minimo_uno(void) {
            string consulta("sp_valida_gen_sd(606, 'SET{300033}', 'SET{}', 'SET{}')");

            TS_ASSERT_EQUALS(1, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 3]: El regimen tiene obligaciones/actividades en la BD, pero en el WS IDC no";
        }

        /**
         * TEST: El régimen tiene obligaciones y actividades
         *
         * @param regimen       612
         * @param roles         SET{}
         * @param obligaciones  SET{}
         * @param actividades   SET{}
         *
         * @return 0 (false)
         */
        void test_op3_regimen_tiene_obligs_y_activs_en_bd_pero_no_en_ws(void) {
            string consulta("sp_valida_gen_sd(612, 'SET{}', 'SET{}', 'SET{}')");

            TS_ASSERT_EQUALS(0, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 3]: El regimen tiene obligaciones/actividades en la BD y en el WS IDC, pero no se intersectan";
        }

        /**
         * TEST: El régimen tiene obligaciones y actividades
         *
         * @param regimen       624
         * @param roles         SET{}
         * @param obligaciones  SET{700,800}
         * @param actividades   SET{200,201,202,203}
         *
         * @return 0 (false)
         */
        void test_op3_regimen_tiene_obligs_y_activs_en_bd_pero_en_ws_no_coinciden_ambos(void) {
            string consulta("sp_valida_gen_sd(624, 'SET{}', 'SET{700,800}', 'SET{200,201,202,203}')");

            TS_ASSERT_EQUALS(0, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 3]: El regimen tiene obligaciones/actividades en la BD y en el WS IDC, además se intersectan solo en las obligaciones";
        }

        /**
         * TEST: El régimen tiene obligaciones y actividades
         *
         * @param regimen       613
         * @param roles         SET{}
         * @param obligaciones  SET{35,36}
         * @param actividades   SET{200,201,202,203}
         *
         * @return 1 (true)
         */
        void test_op3_regimen_tiene_obligs_y_activs_en_bd_y_en_ws_coincide_minimo_una_oblig(void) {
            string consulta("sp_valida_gen_sd(613, 'SET{}', 'SET{35,36}', 'SET{200,201,202,203}')");

            TS_ASSERT_EQUALS(1, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 3]: El regimen tiene obligaciones/actividades en la BD y en el WS IDC, además se intersectan solo en las actividades";
        }

        /**
         * TEST: El régimen tiene obligaciones y actividades
         *
         * @param regimen       613
         * @param roles         SET{}
         * @param obligaciones  SET{40}
         * @param actividades   SET{10,11,12,13,14,15}
         *
         * @return 1 (true)
         */
        void test_op3_regimen_tiene_obligs_y_activs_en_bd_y_en_ws_coincide_minimo_una_activ(void) {
            string consulta("sp_valida_gen_sd(613, 'SET{}', 'SET{40}', 'SET{10,11,12,13,14,15}')");

            TS_ASSERT_EQUALS(1, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 3]: El regimen tiene obligaciones/actividades en la BD y en el WS IDC, además se intersectan en obligaciones y actividades";
        }

        /**
         * TEST: El régimen tiene asignado obligaciones y actividades
         *
         * @param regimen       624
         * @param roles         SET{}
         * @param obligaciones  SET{768}
         * @param actividades   SET{100}
         *
         * @return 1 (true)
         */
        void test_op3_regimen_tiene_obligs_y_activs_en_bd_y_en_ws_coincide_algun_elemento_de_obligs_y_activs(void) {
            string consulta("sp_valida_gen_sd(624, 'SET{}', 'SET{768}', 'SET{100}')");

            TS_ASSERT_EQUALS(1, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 4]: El regimen tiene roles/obligaciones en la BD, pero en el WS IDC no";   
        }

        /**
         * TEST: El régimen tiene asignado roles y obligaciones
         *
         * @param regimen       622
         * @param roles         SET{}
         * @param obligaciones  SET{}
         * @param actividades   SET{}
         *
         * @return 0 (false)
         */
        void test_op4_regimen_tiene_roles_y_obligs_en_bd_pero_no_en_ws(void) {
            string consulta("sp_valida_gen_sd(622, 'SET{}', 'SET{}', 'SET{}')");

            TS_ASSERT_EQUALS(0, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 4]: El regimen tiene roles/obligaciones en la BD y en el WS IDC, pero no se intersectan";
        }

        /**
         * TEST: El régimen tiene asignado roles y obligaciones
         *
         * @param regimen       622
         * @param roles         SET{300521,300522}
         * @param obligaciones  SET{700,701}
         * @param actividades   SET{}
         *
         * @return 0 (false)
         */
        void test_op4_regimen_tiene_roles_y_obligs_en_bd_pero_en_ws_no_coinciden_ambos(void) {
            string consulta("sp_valida_gen_sd(622, 'SET{300521,300522}', 'SET{700,701}', 'SET{}')");

            TS_ASSERT_EQUALS(0, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 4]: El regimen tiene roles/obligaciones en la BD y en el WS IDC, pero solo se intersectan los roles";   
        }

        /**
         * TEST: El régimen tiene asignado roles y obligaciones
         *
         * @param regimen       622
         * @param roles         SET{300525,300526}
         * @param obligaciones  SET{34,35}
         * @param actividades   SET{}
         *
         * @return 1 (true)
         */
        void test_op4_regimen_tiene_roles_y_obligs_en_bd_y_en_ws_coincide_minimo_un_rol(void) {
            string consulta("sp_valida_gen_sd(622, 'SET{300525,300526}', 'SET{34,35}', 'SET{}')");

            TS_ASSERT_EQUALS(1, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 4]: El regimen tiene roles/obligaciones en la BD y en el WS IDC, pero solo se intersectan las obligaciones";
        }

        /**
         * TEST: El régimen tiene asignado roles y obligaciones
         *
         * @param regimen       622
         * @param roles         SET{300033}
         * @param obligaciones  SET{761,762,763}
         * @param actividades   SET{}
         *
         * @return 1 (true)
         */
        void test_op4_regimen_tiene_roles_y_obligs_en_bd_y_en_ws_coincide_minimo_una_oblig(void) {
            string consulta("sp_valida_gen_sd(622, 'SET{300033}', 'SET{761,762,763}', 'SET{}')");

            TS_ASSERT_EQUALS(1, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 4]: El regimen tiene roles/obligaciones en la BD y en el WS IDC, además se intersectan roles y obligaciones";   
        }

        /**
         * TEST: El régimen tiene asignado roles y obligaciones
         *
         * @param regimen       622
         * @param roles         SET{300524}
         * @param obligaciones  SET{792}
         * @param actividades   SET{}
         *
         * @return 1 (true)
         */
        void test_op4_regimen_tiene_roles_y_obligs_en_bd_y_en_ws_coincide_algun_elemento_de_roles_y_obligs(void) {
            string consulta("sp_valida_gen_sd(622, 'SET{300524}', 'SET{792}', 'SET{}')");

            TS_ASSERT_EQUALS(1, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 5]: El regimen tiene roles/obligaciones/actividades en la BD, pero en el WS IDC no";
        }

        /**
         * TEST: El régimen tiene roles, obligaciones y actividades
         *
         * @param regimen       621
         * @param roles         SET{}
         * @param obligaciones  SET{}
         * @param actividades   SET{}
         *
         * @return 0 (false)
         */
        void test_op5_regimen_tiene_roles_obligs_y_activs_en_bd_pero_no_en_ws(void) {
            string consulta("sp_valida_gen_sd(621, 'SET{}', 'SET{}', 'SET{}')");

            TS_ASSERT_EQUALS(0, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 5]: El regimen tiene roles/obligaciones/actividades en la BD y en el WS IDC, pero no se intersectan";
        }

        /**
         * TEST: El régimen tiene roles, obligaciones y actividades
         *
         * @param regimen       621
         * @param roles         SET{300430}
         * @param obligaciones  SET{35,36}
         * @param actividades   SET{200,300,400}
         *
         * @return 0 (false)
         */
        void test_op5_regimen_tiene_roles_obligs_y_activs_en_bd_pero_en_ws_no_coinciden_todos(void) {
            string consulta("sp_valida_gen_sd(621, 'SET{300430}', 'SET{35,36}', 'SET{200,300,400}')");

            TS_ASSERT_EQUALS(0, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 5]: El regimen tiene roles/obligaciones/actividades en la BD y en el WS IDC, pero solo se intersectan roles";
        }

        /**
         * TEST: El régimen tiene roles, obligaciones y actividades
         *
         * @param regimen       621
         * @param roles         SET{300523}
         * @param obligaciones  SET{}
         * @param actividades   SET{}
         *
         * @return 1 (true)
         */
        void test_op5_regimen_tiene_roles_obligs_y_activs_en_bd_y_en_ws_coincide_minimo_un_rol(void) {
            string consulta("sp_valida_gen_sd(621, 'SET{300523}', 'SET{}', 'SET{}')");

            TS_ASSERT_EQUALS(1, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 5]: El regimen tiene roles/obligaciones/actividades en la BD y en el WS IDC, pero solo se intersectan obligaciones";
        }

        /**
         * TEST: El régimen tiene roles, obligaciones y actividades
         *
         * @param regimen       621
         * @param roles         SET{}
         * @param obligaciones  SET{735}
         * @param actividades   SET{}
         *
         * @return 1 (true)
         */
        void test_op5_regimen_tiene_roles_obligs_y_activs_en_bd_y_en_ws_coincide_minimo_una_oblig(void) {
            string consulta("sp_valida_gen_sd(621, 'SET{}', 'SET{735}', 'SET{}')");

            TS_ASSERT_EQUALS(1, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 5]: El regimen tiene roles/obligaciones/actividades en la BD y en el WS IDC, pero solo se intersectan actividades";
        }

        /**
         * TEST: El régimen tiene roles, obligaciones y actividades
         *
         * @param regimen       621
         * @param roles         SET{}
         * @param obligaciones  SET{}
         * @param actividades   SET{50}
         *
         * @return 1 (true)
         */
        void test_op5_regimen_tiene_roles_obligs_y_activs_en_bd_y_en_ws_coincide_minimo_una_activ(void) {
            string consulta("sp_valida_gen_sd(621, 'SET{}', 'SET{}', 'SET{50}')");

            TS_ASSERT_EQUALS(1, ejecutarConsulta(consulta));

            nombre_prueba = "TEST [Operador 5]: El regimen tiene roles/obligaciones/actividades en la BD y en el WS IDC, además se intersectan roles, obligaciones y actividades";
        }

        /**
         * TEST: El régimen tiene roles, obligaciones y actividades
         *
         * @param regimen       621
         * @param roles         SET{300523}
         * @param obligaciones  SET{735}
         * @param actividades   SET{1,10,100}
         *
         * @return 1 (true)
         */
        void test_op5_regimen_tiene_roles_obligs_y_activs_en_bd_y_en_ws_coincide_algun_elemento_de_roles_obligs_y_activs(void) {
            string consulta("sp_valida_gen_sd(621, 'SET{300523}', 'SET{735}', 'SET{1,10,100}')");

            TS_ASSERT_EQUALS(1, ejecutarConsulta(consulta));

            nombre_prueba = "Fin";
        }
};