#include <it.h>
#include <string>
#include <iostream>

using namespace std;

class ClienteBD {
    private:

        ITConnection db_connection;
        ITDBInfo db_info;
        ITQuery *db_query;
        ITRow *db_row;

        bool ejecutarConsulta(string query);

        void liberar();

    public:

        ClienteBD();

        ~ClienteBD();

        bool conectar();

        bool desconectar();

        bool estaConectado();

        bool ejecutarProcedimientoAlmacenado(string stored_procedure);

        string resultado();
};