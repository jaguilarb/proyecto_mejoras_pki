# **Cliente C++ para pruebas del procedimiento almacenado sp_valida_gen_sd**

## Descripción

Cliente C++ para realizar pruebas del procedimiento almacenado **sp_valida_gen_sd** encargado de validar si un regimen puede generar sellos digitales.

## Archivos incluidos

1. El directorio **include** contiene los archivos de cabecera **(archivos.h)**.
    * **ClienteBD.h** definición de la clase que se utiliza para realizar la conexión y ejecución del procedimiento almacenado de la base de datos Informix.
    * **ValidaGeneracionSellos.h** definición de las pruebas unitarias a ejecutar, por lo que hace uso de una instancia de la clase ClienteBD.
2. El directorio **src** contiene los archivos fuente **(archivos.cpp)**.
    * **ClienteBD.cpp** implementación de la clase ClienteBD.
3. El directorio **test** contiene los archivos fuente de las pruebas a ejecutar **(archivos.cpp)**.
    * **ValidaGeneracionSellos.cpp** implementación autogenerada que contiene las pruebas, este archivo no debe modificarse.

## Preparación del entorno

1. **CSDK Informix**
    * Asegurar que se tenga instalado y configurado el CSDK Informix.
    * Para mas detalles consultar el archivo config/README.
2. **CXXTEST**
    * Asegurar que se tenga instalado y configurado el framework CXXTEST.
    * Para mas detalles consultar el archivo config/README.

## Compilación

1. El cliente incluye un archivo Makefile para realizar este proceso, por lo que solo es necesario ejecutar el siguiente comando:
    * > En linux: make compile

## Ejecución

1. El cliente incluye un archivo Makefile para realizar este proceso, por lo que solo es necesario ejecutar el siguiente comando:
    * > En linux: make run

## Limpiar

1. El cliente incluye un archivo Makefile para realizar este proceso, por lo que solo es necesario ejecutar el siguiente comando:
    * > En linux: make clean

## Consideraciones

1. Las instrucciones del documento están probadas en plataformas Linux, aunque debiera funcionar en plataformas Windows, se debe realizar las adecuaciones necesarias para el correcto funcionamiento.