# **Mejoras al proceso de validación para la generación de sellos digitales**

## Descripción

El objetivo del proyecto consiste en realizar mejoras a las operaciones involucradas durante la validación previa a la generación de sellos digitales en la PKI del SAT.

## Archivos incluidos

1. El directorio **config** contiene instrucciones para configurar el entorno de desarrollo.
2. El directorio **sp_cliente** contiene el código fuente del cliente que realiza las pruebas del procedimiento almacenado **sp_valida_gen_sd**.
3. El directorio **sp_cliente_ws** contiene el código fuente del cliente que realiza las pruebas del procedimiento almacenado **sp_valida_gen_sd** utilizando los archivos XML (respuesta del WS IDC) como parámetros del procedimiento.
4. El directorio **sql** contiene los scripts requeridos para la creación y eliminación del procedimiento almacenado **sp_valida_gen_sd**. Así como los scripts para agregar y borrar registros de los catálogos.