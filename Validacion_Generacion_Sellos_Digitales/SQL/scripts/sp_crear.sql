-- Procedimiento almacenado que validar si un regimen recibido permite generar sellos digitales
--
-- Ejecutar: EXECUTE PROCEDURE sp_valida_generacion_sellos(regimen_cve, 'SET{rol_cve_1,...}', 'SET{oblig_cve_1,...}', 'SET{activ_cve_1,...}')
--
-- @param cve_regimen       La clave del regimen a validar
-- @param cve_roles         Las claves de los roles asociados al regimen
-- @param cve_obligaciones  Las claves de las obligaciones asociadas al régimen
-- @param cve_actividades   Las claves de las actividades asociadas al régimen
--
-- @return 					1 puede generar sellos digitales, 0 en caso contrario
--                          Mensaje de exito, Mensaje de error

CREATE PROCEDURE "informix".sp_valida_gen_sd(cve_regimen INTEGER DEFAULT NULL, cve_roles SET(INTEGER NOT NULL) DEFAULT NULL, cve_obligaciones SET(INTEGER NOT NULL) DEFAULT NULL, cve_actividades SET(INTEGER NOT NULL) DEFAULT NULL) 
	RETURNING INTEGER, VARCHAR(255);

    -- Constantes booleanas
	DEFINE true INTEGER;
	DEFINE false INTEGER;

    -- Registros de la base de datos
    DEFINE bd_roles INTEGER;
    DEFINE bd_obligaciones INTEGER;
    DEFINE bd_actividades INTEGER;

    -- Variables de retorno
    DEFINE validacion INTEGER;
    DEFINE mensaje VARCHAR(255);

    LET true  = 1;
    LET false = 0;
    LET validacion = false;

    -- Si la clave del régimen recibida es nula retorna false
    IF (cve_regimen IS NULL) THEN
        RETURN validacion, "Validación INCORRECTA: El régimen recibido es nulo";
    END IF;

    -- Si el régimen no existe en la base de datos retorna false
    IF ((SELECT COUNT(regimen_cve) FROM cat_regimen WHERE regimen_cve = cve_regimen) == 0) THEN
    	RETURN validacion, "Validación INCORRECTA: El régimen recibido no existe en la BD";
    END IF;

    -- Consulta si existen roles, obligaciones y/o actividades asociadas al régimen en la base de datos
    SELECT COUNT(rol_cve) INTO bd_roles FROM cat_rol WHERE regimen_cve = cve_regimen;
    SELECT COUNT(oblig_cve) INTO bd_obligaciones FROM cat_obligacion WHERE regimen_cve = cve_regimen;
    SELECT regimen_act INTO bd_actividades FROM cat_regimen WHERE regimen_cve = cve_regimen;


	-- El régimen no tiene asociado roles/obligaciones/actividades
	IF (bd_roles == 0 AND bd_obligaciones == 0 AND bd_actividades == 0) THEN
		LET validacion = true;
        LET mensaje = "Validación CORRECTA";
	
	-- El régimen tiene asociado roles
	ELIF (bd_roles > 0 AND bd_obligaciones == 0 AND bd_actividades == 0) THEN
		IF ((SELECT COUNT(rol_cve) FROM cat_rol WHERE regimen_cve = cve_regimen AND rol_cve IN (SELECT * FROM TABLE(cve_roles))) > 0) THEN
			LET validacion = true;
            LET mensaje = "Validación CORRECTA";
		ELSE
			LET validacion = false;
            LET mensaje = "Validación INCORRECTA: El régimen no tiene roles comunes en la BD y el WS IDC";
		END IF;

	-- El régimen tiene asociado obligaciones
	ELIF (bd_roles == 0 AND bd_obligaciones > 0 AND bd_actividades == 0) THEN
		IF ((SELECT COUNT(oblig_cve) FROM cat_obligacion WHERE regimen_cve = cve_regimen AND oblig_cve IN (SELECT * FROM TABLE(cve_obligaciones))) > 0) THEN
			LET validacion = true;
            LET mensaje = "Validación CORRECTA";
		ELSE
			LET validacion = false;
            LET mensaje = "Validación INCORRECTA: El régimen no tiene obligaciones comunes en la BD y el WS IDC";
		END IF;

	-- El régimen tiene asociado actividades
	ELIF (bd_roles == 0 AND bd_obligaciones == 0 AND bd_actividades > 0) THEN
		IF ((SELECT COUNT(activ_cve) FROM cat_actividad WHERE activ_cve IN (SELECT * FROM TABLE(cve_actividades))) > 0) THEN
			LET validacion = true;
            LET mensaje = "Validación CORRECTA";
		ELSE
			LET validacion = false;
            LET mensaje = "Validación INCORRECTA: El régimen no tiene actividades comunes en la BD y el WS IDC";
		END IF;

    -- El régimen tiene asociado roles y obligaciones
    ELIF (bd_roles > 0 AND bd_obligaciones > 0 AND bd_actividades == 0) THEN
        IF ((SELECT COUNT(rol_cve) FROM cat_rol WHERE regimen_cve = cve_regimen AND rol_cve IN (SELECT * FROM TABLE(cve_roles))) > 0 OR
        	(SELECT COUNT(oblig_cve) FROM cat_obligacion WHERE regimen_cve = cve_regimen AND oblig_cve IN (SELECT * FROM TABLE(cve_obligaciones))) > 0) THEN
			LET validacion = true;
            LET mensaje = "Validación CORRECTA";
		ELSE
			LET validacion = false;
            LET mensaje = "Validación INCORRECTA: El régimen no tiene roles y obligaciones comunes en la BD y el WS IDC";
		END IF;

	-- El régimen tiene asociado roles y actividades
    ELIF (bd_roles > 0 AND bd_obligaciones == 0 AND bd_actividades > 0) THEN
        IF ((SELECT COUNT(rol_cve) FROM cat_rol WHERE regimen_cve = cve_regimen AND rol_cve IN (SELECT * FROM TABLE(cve_roles))) > 0 OR
        	(SELECT COUNT(activ_cve) FROM cat_actividad WHERE activ_cve IN (SELECT * FROM TABLE(cve_actividades))) > 0) THEN
			LET validacion = true;
            LET mensaje = "Validación CORRECTA";
		ELSE
			LET validacion = false;
            LET mensaje = "Validación INCORRECTA: El régimen no tiene roles y actividades comunes en la BD y el WS IDC";
		END IF;

	-- El régimen tiene asociado obligaciones y actividades
    ELIF (bd_roles == 0 AND bd_obligaciones > 0 AND bd_actividades > 0) THEN
        IF ((SELECT COUNT(oblig_cve) FROM cat_obligacion WHERE regimen_cve = cve_regimen AND oblig_cve IN (SELECT * FROM TABLE(cve_obligaciones))) > 0 OR
        	(SELECT COUNT(activ_cve) FROM cat_actividad WHERE activ_cve IN (SELECT * FROM TABLE(cve_actividades))) > 0) THEN
			LET validacion = true;
            LET mensaje = "Validación CORRECTA";
		ELSE
			LET validacion = false;
            LET mensaje = "Validación INCORRECTA: El régimen no tiene obligaciones y actividades comunes en la BD y el WS IDC";
		END IF;

	-- El régimen tiene asociado roles, obligaciones y actividades
    ELIF (bd_roles > 0 AND bd_obligaciones > 0 AND bd_actividades > 0) THEN
        IF ((SELECT COUNT(rol_cve) FROM cat_rol WHERE regimen_cve = cve_regimen AND rol_cve IN (SELECT * FROM TABLE(cve_roles))) > 0 OR
        	(SELECT COUNT(oblig_cve) FROM cat_obligacion WHERE regimen_cve = cve_regimen AND oblig_cve IN (SELECT * FROM TABLE(cve_obligaciones))) > 0 OR
        	(SELECT COUNT(activ_cve) FROM cat_actividad WHERE activ_cve IN (SELECT * FROM TABLE(cve_actividades))) > 0) THEN
			LET validacion = true;
            LET mensaje = "Validación CORRECTA";
		ELSE
			LET validacion = false;
            LET mensaje = "Validación INCORRECTA: El régimen no tiene roles, obligaciones y actividades comunes en la BD y el WS IDC";
		END IF;
	ELSE
		LET validacion = false;
        LET mensaje = "Validación INCORRECTA: El régimen no tiene definida la combinación de roles, obligaciones y actividades en la BD";
	END IF;
    
	RETURN validacion, mensaje;

END PROCEDURE;
