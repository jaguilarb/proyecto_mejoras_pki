# **Scripts de base de datos**

## Descripción

Conjunto de scripts sql para crear/borrar el procedimiento almacenado **sp_valida_gen_sd** en la base de datos ar_sat. El procedimiento almacenado valida si un regimen (perteneciente a un RFC de un contribuyente) puede generar sellos digitales.

Además contiene los scripts sql para borrar/insertar los registros de los catálogos **cat_regimen**, **cat_rol**, **cat_obligacion** y **cat_actividad**.

## Archivos incluidos

1. sp_valida_gen_sd_create.sql
    - Script para crear el procedimiento almacenado **sp_valida_gen_sd**.
2. sp_valida_gen_sd_drop.sql
    - Script para borrar el procedimiento almacenado **sp_valida_gen_sd**.
3. catalogos_gen_sd_delete.sql
    - Script para borrar todos los registros de los catálogos.
4. catalogos_gen_sd_insert.sql
    - Script para agregar todos los registros de los catálogos.
5. catalogos_gen_sd_insert_prev.sql
    - Script para agregar todos los registros de los catálogos, correspondientes a la estructura de las tablas que se está en producción.
6. catalogos_gen_sd_update.sql
    - Script para actualizar los registros del catálogo de regímenes por el cambio de estructura de la tabla.

## Ejecución del script
1. Copiar el script al servidor de base de datos informix.
    - > scp script.sql  user@host-informix-server:script.sql
2. Cambiar los permisos al script (desde el servidor de base de datos informix).
    - > chown informix:informix script.sql
3. Convertir el script a formato unix.
    - > dos2unix script.sql
4. Ejecutar el script desde la línea de comandos.
    - > dbaccess ar_sat script.sql

## Scrip sp_valida_gen_sd_create.sql

1. Ejecutar los pasos descritos en la sección previa.
2. Adicionalmente se debe asignar permisos de ejecución del procedimiento almacenado a otros usuarios (desde el cliente dbaccess).
    - > GRANT EXECUTE ON sp_valida_gen_sd to ar_sat;