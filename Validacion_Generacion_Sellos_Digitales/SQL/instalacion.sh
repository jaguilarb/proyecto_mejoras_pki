#!/bin/bash 

#*********************************************************************************************************************
#   Script para implementar los cambios requeridos para instalar el procedimiento almacenado que valida las obligaciones
#   de un RFC al solicitar la generación de sellos digitales
#
#   Objetivos:
#               - Respaldar la estructura actual de la tabla cat_regimen
#               - Respaldar los datos actuales de los catalogos (cat_regimen, cat_rol, cat_obligacion y cat_actividad)
#               - Borrar los datos actuales de los catalogos (cat_regimen, cat_rol, cat_obligacion y cat_actividad)
#               - Actualizar la estructura de la tabla cat_regimen
#               - Cargar los datos a los catalogos (cat_regimen, cat_rol, cat_obligacion y cat_actividad)
#               - Instalar el procedimiento almacenado sp_valida_gen_sd
#
#   Consideraciones:    En caso de fallo de un paso, se deshacen los cambios previos si se requieren.
#
#   Requisitos: Ninguno
#
#**********************************************************************************************************************

# Directorios
DIRECTORIO_RESPALDOS="respaldos"
DIRECTORIO_SCRIPTS="scripts"


# Scripts
ARCHIVO_CAT_REGIMEN_RESPALDO_ESTRUCTURA="cat_regimen_respaldo_estructura.sql"
ARCHIVO_CAT_REGIMEN_ACTUALIZAR_ESTRUCTURA="cat_regimen_actualizar_estructura.sql"
ARCHIVO_CAT_REGIMEN_ELIMINAR="cat_regimen_eliminar.sql"
ARCHIVO_CAT_RESPALDAR_DATOS="cat_respaldar_datos.sql"
ARCHIVO_CAT_BORRAR_DATOS="cat_borrar_datos.sql"
ARCHIVO_CAT_CARGAR_DATOS="cat_cargar_datos.sql"
ARCHIVO_CAT_CARGAR_DATOS_PREVIOS="cat_cargar_datos_previos.sql"
ARCHIVO_SP_INSTALAR="sp_crear.sql"
ARCHIVO_SP_ELIMINAR="sp_eliminar.sql"
ARCHIVO_SP_PERMISOS="sp_permisos.sql"


# Parámetros de la base de datos
BD_NOMBRE="ar_sat"
BD_CAT_REGIMEN="cat_regimen"


# Crea el directorio para los respaldos en caso de no existir
if [ ! -d $DIRECTORIO_RESPALDOS ]; then
    echo "Creación del directorio de respaldos........................[ INICIADO   ]"

    mkdir -p $DIRECTORIO_RESPALDOS

    echo "Creación del directorio de respaldos........................[ FINALIZADO ]"
fi


# Convierte los scripts a utilizar a formato Unix
echo "Conversión de scripts a formato Unix........................[ INICIADO   ]"

if ! type dos2unix ; then
    echo "Conversión de scripts a formato Unix........................[ FALLIDO    ]" 

else
    for script in $DIRECTORIO_SCRIPTS/*.sql; do
        dos2unix $script
    done

    echo "Conversión de scripts a formato Unix........................[ FINALIZADO ]" 
fi


# Cambia el propietario de los scripts a utilizar
echo "Cambio de propietario de los scripts........................[ INICIADO   ]" 

if ! type chown ; then
    echo "Cambio de propietario de los scripts........................[ FALLIDO    ]" 

else
    for script in $DIRECTORIO_SCRIPTS/*.sql; do
        chown informix:informix $script
    done

    echo "Cambio de propietario de los scripts........................[ FINALIZADO ]" 
fi


# Respalda la estructura actual de la tabla cat_regimen
echo "Respaldo de la estructura de la tabla cat_regimen...........[ INICIADO   ]" 

if ! type dbschema ; then
    echo "Respaldo de la estructura de la tabla cat_regimen...........[ FALLIDO    ]" 

    exit 1
else
    dbschema -d $BD_NOMBRE -t $BD_CAT_REGIMEN -q > "$DIRECTORIO_RESPALDOS/$ARCHIVO_CAT_REGIMEN_RESPALDO_ESTRUCTURA"

    echo "Respaldo de la estructura de la tabla cat_regimen...........[ FINALIZADO ]" 
fi


# Valida que se pueda utilizar el comando dbaccess
echo "Validación del acceso al comando dbaccess...................[ INICIADO   ]"

if ! type dbaccess ; then
    echo "Validación del acceso al comando dbaccess...................[ FALLIDO    ]"

    exit 1
fi

echo "Validación del acceso al comando dbaccess...................[ FINALIZADO ]"


# Respalda los datos actuales de los catálogos (cat_regimen, cat_rol, cat_obligacion y cat_actividad)
echo "Respaldo de los datos de los catalogos......................[ INICIADO   ]" 

if ! dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_RESPALDAR_DATOS" ; then
    echo "Respaldo de los datos de los catalogos......................[ FALLIDO    ]" 

    exit 1
else
    echo "Respaldo de los datos de los catalogos......................[ FINALIZADO ]" 
fi


# Borra los datos actuales de los catálogos (cat_regimen, cat_rol, cat_obligacion y cat_actividad)
echo "Borrado de los datos de los catalogos.......................[ INICIADO   ]" 

if ! dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_BORRAR_DATOS" ; then
    echo "Borrado de los datos de los catalogos.......................[ FALLIDO    ]" 

    exit 1
else
    echo "Borrado de los datos de los catalogos.......................[ FINALIZADO ]" 
fi


# Actualiza la estructura actual de la tabla cat_regimen
echo "Actualizado de la estructura de la tabla cat_regimen........[ INICIADO   ]" 

if ! dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_REGIMEN_ACTUALIZAR_ESTRUCTURA" ; then
    echo "Actualizado de la estructura de la tabla cat_regimen........[ FALLIDO    ]" 

    # Deshace los cambios realizados previamente

    #   1. Carga los datos de los catalogos, con el respaldo creado
    echo "Deshaciendo el borrado de los datos de los catalogos........[ INICIADO   ]" 

    dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_CARGAR_DATOS_PREVIOS" 

    echo "Deshaciendo el borrado de los datos de los catalogos...... .[ FINALIZADO ]" 

    exit 1
else
    echo "Actualizado de la estructura de la tabla cat_regimen........[ FINALIZADO ]" 
fi


# Carga los datos de los catalogos considerando la nueva estructura de la tabla cat_regimen
echo "Cargado de los datos de los catalogos.......................[ INICIADO   ]" 

if ! dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_CARGAR_DATOS" ; then
    echo "Cargado de los datos de los catalogos.......................[ FALLIDO    ]" 

    # Deshace los cambios realizados previamente

    #   1. Regresa la estructura inicial de la tabla cat_regimen
    #       1.1 Borra la tabla cat_regimen
    #       1.2 Crea la tabla cat_regimen a partir del script de respaldo de la estructura
    echo "Deshaciendo el cambio de estructura de cat_regimen..........[ INICIADO   ]" 

    dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_REGIMEN_ELIMINAR" 
    dbaccess $BD_NOMBRE "$DIRECTORIO_RESPALDOS/$ARCHIVO_CAT_REGIMEN_RESPALDO_ESTRUCTURA" 

    echo "Deshaciendo el cambio de estructura de cat_regimen..........[ FINALIZADO ]" 
    
    #   2. Carga los datos de los catalogos, con el respaldo creado
    echo "Deshaciendo el borrado de los datos de los catalogos........[ INICIADO   ]" 

    dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_CARGAR_DATOS_PREVIOS" 

    echo "Deshaciendo el borrado de los datos de los catalogos...... .[ FINALIZADO ]" 

    exit 1
else
    echo "Cargado de los datos de los catalogos.......................[ FINALIZADO ]" 
fi


# Crea el procedimiento almacenado sp_valida_gen_sd
echo "Instalando el procedimiento almacenado......................[ INICIADO   ]" 

if ! dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_SP_INSTALAR" ; then
    echo "Instalando el procedimiento almacenado......................[ FALLIDO    ]" 

    # Deshace los cambios realizados previamente

    #   1. Borra los datos de los catalogos
    echo "Deshaciendo la carga de los datos de los catalogos..........[ INICIADO   ]" 

    dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_BORRAR_DATOS" 

    echo "Deshaciendo la carga de los datos de los catalogos..........[ FINALIZADO ]" 

    #   2. Regresa la estructura inicial de la tabla cat_regimen
    #       2.1 Borra la tabla cat_regimen
    #       2.2 Crea la tabla cat_regimen a partir del script de respaldo de la estructura
    echo "Deshaciendo el cambio de estructura de cat_regimen..........[ INICIADO   ]" 

    dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_REGIMEN_ELIMINAR" 
    dbaccess $BD_NOMBRE "$DIRECTORIO_RESPALDOS/$ARCHIVO_CAT_REGIMEN_RESPALDO_ESTRUCTURA" 

    echo "Deshaciendo el cambio de estructura de cat_regimen..........[ FINALIZADO ]" 
    
    #   3. Carga los datos de los catalogos, con el respaldo creado
    echo "Deshaciendo el borrado de los datos de los catalogos........[ INICIADO   ]" 

    dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_CARGAR_DATOS_PREVIOS" 

    echo "Deshaciendo el borrado de los datos de los catalogos...... .[ FINALIZADO ]" 

    exit 1
else
    echo "Instalando el procedimiento almacenado......................[ FINALIZADO ]" 
fi


# Asigna permisos de ejecución al procedimiento almacenado sp_valida_gen_sd
echo "Asignando permisos al procedimiento almacenado..............[ INICIADO   ]" 

if ! dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_SP_PERMISOS" ; then
    echo "Asignando permisos al procedimiento almacenado..............[ FALLIDO    ]" 

    # Deshace los cambios realizados previamente

    #   1. Elimina el procedimiento almacenado
    echo "Deshaciendo la instalación del procedimiento almacenado.....[ INICIADO   ]" 

    dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_SP_ELIMINAR" 

    echo "Deshaciendo la instalación del procedimiento almacenado.....[ FINALIZADO ]" 

    #   2. Borra los datos de los catalogos
    echo "Deshaciendo la carga de los datos de los catalogos..........[ INICIADO   ]" 

    dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_BORRAR_DATOS" 

    echo "Deshaciendo la carga de los datos de los catalogos..........[ FINALIZADO ]" 

    #   3. Regresa la estructura inicial de la tabla cat_regimen
    #       3.1 Borra la tabla cat_regimen
    #       3.2 Crea la tabla cat_regimen a partir del script de respaldo de la estructura
    echo "Deshaciendo el cambio de estructura de cat_regimen..........[ INICIADO   ]" 

    dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_REGIMEN_ELIMINAR" 
    dbaccess $BD_NOMBRE "$DIRECTORIO_RESPALDOS/$ARCHIVO_CAT_REGIMEN_RESPALDO_ESTRUCTURA" 

    echo "Deshaciendo el cambio de estructura de cat_regimen..........[ FINALIZADO ]" 
    
    #   4. Carga los datos de los catalogos, con el respaldo creado
    echo "Deshaciendo el borrado de los datos de los catalogos........[ INICIADO   ]" 

    dbaccess $BD_NOMBRE "$DIRECTORIO_SCRIPTS/$ARCHIVO_CAT_CARGAR_DATOS_PREVIOS" 

    echo "Deshaciendo el borrado de los datos de los catalogos...... .[ FINALIZADO ]" 

    exit 1
else
    echo "Asignando permisos al procedimiento almacenado..............[ FINALIZADO ]" 
fi