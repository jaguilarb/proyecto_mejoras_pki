# **Configuraciones del entorno de desarrollo**

## CSDK Informix

### Descripción

API para el desarrollo de aplicaciones que acceden a bases de datos Informix.

### Instalación

Puede seguir la guía de instalación descrita en la [documentación oficial](http://publib.boulder.ibm.com/infocenter/idshelp/v10/topic/com.ibm.cpi.doc/CLIENT_wrapper.htm).

### Configuración

1. Configurar las variables de entorno.
	* INFORMIXDIR, corresponde al directorio de instalación del CSDK Informix.
		* > Por ejemplo en linux: export INFORMIXDIR=/opt/IBM/informix
	* INFORMIXSERVER, corresponde a la instancia de la base de datos a utilizar.
		* > Por ejemplo en linux: export INFORMIXSERVER=pki_ar
	* LD_LIBRARY_PATH, ruta de las bibliotecas dinámicas utilizadas por las aplicaciones.
		* > Por ejemplo en linux: export LD_LIBRARY_PATH=$INFORMIXDIR/lib/c++:$INFORMIXDIR/lib/dmi:$INFORMIXDIR/lib/esql:$INFORMIX/lib/client:$INFORMIXDIR/lib/cli:$INFORMIXDIR/lib
	* Agregar al PATH el directorio $INFORMIX/bin.
		* > Por ejemplo en linux: export PATH=$PATH:$INFORMIXDIR/bin
2. Configuración del host del servidor de base de datos Informix.
	* Agregar al archivo hosts de la plataforma correspondiente la dirección IP del servidor de base de datos.
		* > Por ejemplo en linux (agregar al archivo /etc/hosts): 172.16.122.23 informix-server
3. Configuración de la conexión a la base de datos Informix.
	* Agregar al archivo $INFORMIXDIR/etc/sqlhosts la configuración de la conexión.
		* > Por ejemplo en linux: pki_ar     onsoctcp    informix-server     1515

### Consideraciones

Consultar la [documentación oficial](http://publib.boulder.ibm.com/infocenter/idshelp/v10/index.jsp).

## CXXTEST

### Descripción

Framework para pruebas unitarias C++.

### Instalación

Puede seguir la guía de instalación descrita en la [documentación oficial](http://cxxtest.com/guide.html#installation).

### Configuración

1. Configurar las variables de entorno.
	* CXXTEST, corresponde al directorio de instalación del framework CXXTEST.
		* > Por ejemplo en linux: export CXXTEST=/opt/cxxtest
	* Agregar al PATH el directorio $CXXTEST/bin.
		* > Por ejemplo en linux: export PATH=$PATH:$CXXTEST/bin

### Consideraciones

Consultar la [documentación oficial](http://cxxtest.com/guide.html).