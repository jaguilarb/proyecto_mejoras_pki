#include <cxxtest/TestSuite.h>
#include <ClienteBD.h>
#include <ClienteSOAP.h>
#include <string>
#include <stdlib.h>
#include <iostream>

class ValidaGeneracionSellosWS : public CxxTest::TestSuite {

    private:

        ClienteBD *bd = NULL;
        ClienteSOAP *soap = NULL;

        string nombre_prueba;

        int ejecutarConsulta(string consulta) {
            if(bd == NULL) {
                TS_FAIL("No se puede ejecutar la consulta porque el objeto bd es nulo");

                return 0;
            }

            if(!bd->estaConectado()) {
                TS_FAIL("No se puede ejecutar la consulta porque no hay conexión a la BD");

                return 0;
            }

            if(!bd->ejecutarProcedimientoAlmacenado(consulta)) {
                TS_FAIL("Error al ejecutar el procedimiento almacenado");

                return 0;
            }

            return atoi(bd->resultado().c_str());
        }

        string obtenerRegimenes() {
            if(soap == NULL) {
                TS_FAIL("No se pueden obtener los regimenes porque el objeto soap es nulo");

                return string("");
            }
            if(!soap->estaCargado()) {
                TS_FAIL("No se pueden obtener los regimenes porque no se ha cargado el archivo XML");

                return string("");
            }

            string regimenes = soap->regimenes();

            cout << "INFO: Regimenes WS IDC [" << regimenes << "].\n";

            return regimenes;
        }

        string obtenerRoles() {
            if(soap == NULL) {
                TS_FAIL("No se pueden obtener los roles porque el objeto soap es nulo");

                return string("");
            }
            if(!soap->estaCargado()) {
                TS_FAIL("No se pueden obtener los roles porque no se ha cargado el archivo XML");

                return string("");
            }

            string roles = soap->roles();

            cout << "INFO: Roles WS IDC [" << roles << "].\n";

            return roles;
        }

        string obtenerObligaciones() {
            if(soap == NULL) {
                TS_FAIL("No se pueden obtener las obligaciones porque el objeto soap es nulo");

                return string("");
            }
            if(!soap->estaCargado()) {
                TS_FAIL("No se pueden obtener las obligaciones porque no se ha cargado el archivo XML");

                return string("");
            }

            string obligaciones = soap->obligaciones();

            cout << "INFO: Obligaciones WS IDC [" << obligaciones << "].\n";

            return obligaciones;
        }

        string obtenerActividades() {
            if(soap == NULL) {
                TS_FAIL("No se pueden obtener las actividades porque el objeto soap es nulo");

                return string("");
            }
            if(!soap->estaCargado()) {
                TS_FAIL("No se pueden obtener las actividades porque no se ha cargado el archivo XML");

                return string("");
            }

            string actividades = soap->actividades();

            cout << "INFO: Actividades WS IDC [" << actividades << "].\n";

            return actividades;
        }

        string obtenerRegimenPorIndice(string regimenes, int indice) {
            int pos = 0;
            int i = 0;
            string temporal = regimenes;

            while(i < indice) {
                pos = temporal.find(",", 0, 1);
                temporal = temporal.substr(pos + 1);
                i++;
            }

            int pos_fin =  temporal.find(",", 0, 1);

            if(pos_fin == string::npos) {
                pos_fin = regimenes.length();
            }

            return temporal.substr(0, pos_fin);
        }

    public:

        void setUp() {
            nombre_prueba = nombre_prueba.empty() ? string("\n\nTEST: Validación para la generación de sellos digitales del RFC SEC570311D22") : string("\n" + nombre_prueba);

            cout << nombre_prueba;
            
            bd = new ClienteBD();
            bd->conectar();

            soap = new ClienteSOAP();

        }

        void tearDown() {
            bd->desconectar();
            bd = NULL;

            soap->liberar();
            soap = NULL;

            if(nombre_prueba == "Fin") {
                cout << "\n";
            }
        }

        /**
         * TEST: Respuesta IDC sin regimenes
         */
        void test_ws_idc_sin_regimenes(void) {
            string archivo = "resources/SEC570311D22_WS_IDC_Response.xml";

            bool puede_generar_sd = false;

            if(!soap->cargar(archivo)) {
                TS_FAIL("No se pudo cargar el archivo " + archivo);
            }

            string regimenes = obtenerRegimenes();
            
            if(soap->numRegimenes() == 0) {
                TS_ASSERT_EQUALS(false, puede_generar_sd);
            }
            else {
                string roles = obtenerRoles();
                string obligaciones = obtenerObligaciones();
                string actividades = obtenerActividades();

                string q_roles = "'SET{" + roles + "}'";
                string q_obligaciones = "'SET{" + obligaciones + "}'";
                string q_actividades = "'SET{" + actividades + "}'";

                int i;

                for(i = 0; i < soap->numRegimenes(); i++) {
                    string q_regimen = obtenerRegimenPorIndice(regimenes, i);

                    string consulta = "sp_valida_gen_sd(" + q_regimen + ", " + q_roles + ", " + q_obligaciones + ", " + q_actividades + ")";
                    
                    puede_generar_sd = puede_generar_sd || ejecutarConsulta(consulta);

                    if(puede_generar_sd) {
                        break;
                    }
                }

                TS_ASSERT_EQUALS(true, puede_generar_sd);
            }

            nombre_prueba = "TEST: Validación para la generación de sellos digitales del RFC CACX871014U63";
        }

        /**
         * TEST: Respuesta IDC con un regimen no existente en la BD
         */
        void test_ws_idc_con_un_regimen_no_existente_en_la_bd(void) {
            string archivo = "resources/CACX871014U63_WS_IDC_Response_1.xml";

            bool puede_generar_sd = false;

            if(!soap->cargar(archivo)) {
                TS_FAIL("No se pudo cargar el archivo " + archivo);
            }

            string regimenes = obtenerRegimenes();
            
            if(soap->numRegimenes() == 0) {
                TS_ASSERT_EQUALS(false, puede_generar_sd);
            }
            else {
                string roles = obtenerRoles();
                string obligaciones = obtenerObligaciones();
                string actividades = obtenerActividades();

                string q_roles = "'SET{" + roles + "}'";
                string q_obligaciones = "'SET{" + obligaciones + "}'";
                string q_actividades = "'SET{" + actividades + "}'";

                int i;

                for(i = 0; i < soap->numRegimenes(); i++) {
                    string q_regimen = obtenerRegimenPorIndice(regimenes, i);

                    string consulta = "sp_valida_gen_sd(" + q_regimen + ", " + q_roles + ", " + q_obligaciones + ", " + q_actividades + ")";
                    
                    puede_generar_sd = puede_generar_sd || ejecutarConsulta(consulta);

                    if(puede_generar_sd) {
                        break;
                    }
                }

                TS_ASSERT_EQUALS(false, puede_generar_sd);
            }

            nombre_prueba = "TEST: Validación para la generación de sellos digitales del RFC CACX871014U63";
        }

        /**
         * TEST: Respuesta IDC con un regimen existente en la bd
         */
        void test_ws_idc_con_un_regimen(void) {
            string archivo = "resources/CACX871014U63_WS_IDC_Response_2.xml";

            bool puede_generar_sd = false;

            if(!soap->cargar(archivo)) {
                TS_FAIL("No se pudo cargar el archivo " + archivo);
            }

            string regimenes = obtenerRegimenes();
            
            if(soap->numRegimenes() == 0) {
                TS_ASSERT_EQUALS(false, puede_generar_sd);
            }
            else {
                string roles = obtenerRoles();
                string obligaciones = obtenerObligaciones();
                string actividades = obtenerActividades();

                string q_roles = "'SET{" + roles + "}'";
                string q_obligaciones = "'SET{" + obligaciones + "}'";
                string q_actividades = "'SET{" + actividades + "}'";

                int i;

                for(i = 0; i < soap->numRegimenes(); i++) {
                    string q_regimen = obtenerRegimenPorIndice(regimenes, i);

                    string consulta = "sp_valida_gen_sd(" + q_regimen + ", " + q_roles + ", " + q_obligaciones + ", " + q_actividades + ")";
                    
                    puede_generar_sd = puede_generar_sd || ejecutarConsulta(consulta);

                    if(puede_generar_sd) {
                        break;
                    }
                }

                TS_ASSERT_EQUALS(true, puede_generar_sd);
            }

            nombre_prueba = "TEST: Validación para la generación de sellos digitales del RFC RORS790714TH0";
        }

        /**
         * TEST: Respuesta IDC con mas de un regimen todos invalidos
         */
        void test_ws_idc_con_mas_de_un_regimen_todos_invalidos(void) {
            string archivo = "resources/RORS790714TH0_WS_IDC_Response_1.xml";

            bool puede_generar_sd = false;

            if(!soap->cargar(archivo)) {
                TS_FAIL("No se pudo cargar el archivo " + archivo);
            }

            string regimenes = obtenerRegimenes();
            
            if(soap->numRegimenes() == 0) {
                TS_ASSERT_EQUALS(false, puede_generar_sd);
            }
            else {
                string roles = obtenerRoles();
                string obligaciones = obtenerObligaciones();
                string actividades = obtenerActividades();

                string q_roles = "'SET{" + roles + "}'";
                string q_obligaciones = "'SET{" + obligaciones + "}'";
                string q_actividades = "'SET{" + actividades + "}'";

                int i;

                for(i = 0; i < soap->numRegimenes(); i++) {
                    string q_regimen = obtenerRegimenPorIndice(regimenes, i);

                    string consulta = "sp_valida_gen_sd(" + q_regimen + ", " + q_roles + ", " + q_obligaciones + ", " + q_actividades + ")";
                    
                    puede_generar_sd = puede_generar_sd || ejecutarConsulta(consulta);

                    if(puede_generar_sd) {
                        break;
                    }
                }

                TS_ASSERT_EQUALS(false, puede_generar_sd);
            }

            nombre_prueba = "TEST: Validación para la generación de sellos digitales del RFC RORS790714TH0";
        }

        /**
         * TEST: Respuesta IDC con mas de un regimen no todos validos
         */
        void test_ws_idc_con_mas_de_un_regimen_no_todos_validos(void) {
            string archivo = "resources/RORS790714TH0_WS_IDC_Response_2.xml";

            bool puede_generar_sd = false;

            if(!soap->cargar(archivo)) {
                TS_FAIL("No se pudo cargar el archivo " + archivo);
            }

            string regimenes = obtenerRegimenes();
            
            if(soap->numRegimenes() == 0) {
                TS_ASSERT_EQUALS(false, puede_generar_sd);
            }
            else {
                string roles = obtenerRoles();
                string obligaciones = obtenerObligaciones();
                string actividades = obtenerActividades();

                string q_roles = "'SET{" + roles + "}'";
                string q_obligaciones = "'SET{" + obligaciones + "}'";
                string q_actividades = "'SET{" + actividades + "}'";

                int i;

                for(i = 0; i < soap->numRegimenes(); i++) {
                    string q_regimen = obtenerRegimenPorIndice(regimenes, i);

                    string consulta = "sp_valida_gen_sd(" + q_regimen + ", " + q_roles + ", " + q_obligaciones + ", " + q_actividades + ")";
                    
                    puede_generar_sd = puede_generar_sd || ejecutarConsulta(consulta);

                    if(puede_generar_sd) {
                        break;
                    }
                }

                TS_ASSERT_EQUALS(true, puede_generar_sd);
            }

            nombre_prueba = "TEST: Validación para la generación de sellos digitales del RFC RORS790714TH0";
        }

        /**
         * TEST: Respuesta IDC con mas de un regimen todos validos
         */
        void test_ws_idc_con_mas_de_un_regimen_todos_validos(void) {
            string archivo = "resources/RORS790714TH0_WS_IDC_Response_3.xml";

            bool puede_generar_sd = false;

            if(!soap->cargar(archivo)) {
                TS_FAIL("No se pudo cargar el archivo " + archivo);
            }

            string regimenes = obtenerRegimenes();
            
            if(soap->numRegimenes() == 0) {
                TS_ASSERT_EQUALS(false, puede_generar_sd);
            }
            else {
                string roles = obtenerRoles();
                string obligaciones = obtenerObligaciones();
                string actividades = obtenerActividades();

                string q_roles = "'SET{" + roles + "}'";
                string q_obligaciones = "'SET{" + obligaciones + "}'";
                string q_actividades = "'SET{" + actividades + "}'";

                int i;

                for(i = 0; i < soap->numRegimenes(); i++) {
                    string q_regimen = obtenerRegimenPorIndice(regimenes, i);

                    string consulta = "sp_valida_gen_sd(" + q_regimen + ", " + q_roles + ", " + q_obligaciones + ", " + q_actividades + ")";
                    
                    puede_generar_sd = puede_generar_sd || ejecutarConsulta(consulta);

                    if(puede_generar_sd) {
                        break;
                    }
                }

                TS_ASSERT_EQUALS(true, puede_generar_sd);
            }

            nombre_prueba = "Fin";
        }
};