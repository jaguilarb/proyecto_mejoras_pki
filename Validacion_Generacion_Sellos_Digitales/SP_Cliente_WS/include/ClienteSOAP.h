#include <CkXml.h>
#include <string>
#include <iostream>

using namespace std;

class ClienteSOAP {
    private:

        CkXml soap_xml;

        bool esta_cargado;

        int n_regimenes;
        int n_roles;
        int n_obligaciones;
        int n_actividades;

        void inicializar();

    public:

        ClienteSOAP();

        ~ClienteSOAP();

        bool cargar(string);

        bool liberar();

        bool estaCargado();

        string regimenes();

        string roles();

        string obligaciones();

        string actividades();

        int numRegimenes();

        int numRoles();

        int numObligaciones();

        int numActividades();
};