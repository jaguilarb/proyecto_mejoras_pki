/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#define _CXXTEST_HAVE_STD
#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/ErrorPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::ErrorPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "cxxtest";
    status = CxxTest::Main< CxxTest::ErrorPrinter >( tmp, argc, argv );
    return status;
}
bool suite_ValidaGeneracionSellosWS_init = false;
#include "/home/jaguilar/Azertia/PKI/Proyecto_Mejoras_PKI/Validacion_Generacion_Sellos_Digitales/sp_cliente_ws/include/ValidaGeneracionSellosWS.h"

static ValidaGeneracionSellosWS suite_ValidaGeneracionSellosWS;

static CxxTest::List Tests_ValidaGeneracionSellosWS = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_ValidaGeneracionSellosWS( "include/ValidaGeneracionSellosWS.h", 8, "ValidaGeneracionSellosWS", suite_ValidaGeneracionSellosWS, Tests_ValidaGeneracionSellosWS );

static class TestDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_sin_regimenes : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_sin_regimenes() : CxxTest::RealTestDescription( Tests_ValidaGeneracionSellosWS, suiteDescription_ValidaGeneracionSellosWS, 164, "test_ws_idc_sin_regimenes" ) {}
 void runTest() { suite_ValidaGeneracionSellosWS.test_ws_idc_sin_regimenes(); }
} testDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_sin_regimenes;

static class TestDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_un_regimen_no_existente_en_la_bd : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_un_regimen_no_existente_en_la_bd() : CxxTest::RealTestDescription( Tests_ValidaGeneracionSellosWS, suiteDescription_ValidaGeneracionSellosWS, 210, "test_ws_idc_con_un_regimen_no_existente_en_la_bd" ) {}
 void runTest() { suite_ValidaGeneracionSellosWS.test_ws_idc_con_un_regimen_no_existente_en_la_bd(); }
} testDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_un_regimen_no_existente_en_la_bd;

static class TestDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_un_regimen : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_un_regimen() : CxxTest::RealTestDescription( Tests_ValidaGeneracionSellosWS, suiteDescription_ValidaGeneracionSellosWS, 256, "test_ws_idc_con_un_regimen" ) {}
 void runTest() { suite_ValidaGeneracionSellosWS.test_ws_idc_con_un_regimen(); }
} testDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_un_regimen;

static class TestDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_mas_de_un_regimen_todos_invalidos : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_mas_de_un_regimen_todos_invalidos() : CxxTest::RealTestDescription( Tests_ValidaGeneracionSellosWS, suiteDescription_ValidaGeneracionSellosWS, 302, "test_ws_idc_con_mas_de_un_regimen_todos_invalidos" ) {}
 void runTest() { suite_ValidaGeneracionSellosWS.test_ws_idc_con_mas_de_un_regimen_todos_invalidos(); }
} testDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_mas_de_un_regimen_todos_invalidos;

static class TestDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_mas_de_un_regimen_no_todos_validos : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_mas_de_un_regimen_no_todos_validos() : CxxTest::RealTestDescription( Tests_ValidaGeneracionSellosWS, suiteDescription_ValidaGeneracionSellosWS, 348, "test_ws_idc_con_mas_de_un_regimen_no_todos_validos" ) {}
 void runTest() { suite_ValidaGeneracionSellosWS.test_ws_idc_con_mas_de_un_regimen_no_todos_validos(); }
} testDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_mas_de_un_regimen_no_todos_validos;

static class TestDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_mas_de_un_regimen_todos_validos : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_mas_de_un_regimen_todos_validos() : CxxTest::RealTestDescription( Tests_ValidaGeneracionSellosWS, suiteDescription_ValidaGeneracionSellosWS, 394, "test_ws_idc_con_mas_de_un_regimen_todos_validos" ) {}
 void runTest() { suite_ValidaGeneracionSellosWS.test_ws_idc_con_mas_de_un_regimen_todos_validos(); }
} testDescription_suite_ValidaGeneracionSellosWS_test_ws_idc_con_mas_de_un_regimen_todos_validos;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";
