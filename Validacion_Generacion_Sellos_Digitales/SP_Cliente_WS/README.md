# **Cliente C++ para pruebas del procedimiento almacenado sp_valida_gen_sd utilizando la respuesta del WS IDC**

## Descripción

Cliente C++ para realizar pruebas del procedimiento almacenado **sp_valida_gen_sd** encargado de validar si un regimen puede generar sellos digitales. Los parámetros enviados al procedimiento se obtienen de la respuesta del WS IDC.

La respuesta del WS IDC se simula utilizando los archivos XML retornados por este.

## Archivos incluidos

1. El directorio **include** contiene los archivos de cabecera **(archivos.h)**.
    * **ClienteBD.h** definición de la clase que se utiliza para realizar la conexión y ejecución del procedimiento almacenado de la base de datos Informix.
    * **ClienteSOAP.h** definición de la clase que se utiliza para procesar el archivo XML (respuesta del WS IDC).
    * **ValidaGeneracionSellosWS.h** definición de las pruebas unitarias a ejecutar.
2. El directorio **src** contiene los archivos fuente **(archivos.cpp)**.
    * **ClienteBD.cpp** implementación de la clase ClienteBD.
    * **ClienteSOAP.cpp** implementación de la clase ClienteSOAP.
3. El directorio **test** contiene los archivos fuente de las pruebas a ejecutar **(archivos.cpp)**.
    * **ValidaGeneracionSellosWS.cpp** implementación autogenerada que contiene las pruebas, este archivo no debe modificarse.
4. El directorio **lib** contiene la biblioteca **chilkat** utilizada para procesar archivos XML.
5. El directorio **resources** contiene los archivos XML utilizados para las pruebas.

## Preparación del entorno

1. **CSDK Informix**
    * Asegurar que se tenga instalado y configurado el CSDK Informix.
    * Para mas detalles consultar el archivo config/README.
2. **CXXTEST**
    * Asegurar que se tenga instalado y configurado el framework CXXTEST.
    * Para mas detalles consultar el archivo config/README.
3. **Biblioteca Chilkat**
    * Consultar la documentación en la [página oficial](http://www.chilkatsoft.com/chilkatLinux.asp).

## Compilación

1. El cliente incluye un archivo Makefile para realizar este proceso, por lo que solo es necesario ejecutar el siguiente comando:
    * > En linux: make compile

## Ejecución

1. El cliente incluye un archivo Makefile para realizar este proceso, por lo que solo es necesario ejecutar el siguiente comando:
    * > En linux: make run

## Limpiar

1. El cliente incluye un archivo Makefile para realizar este proceso, por lo que solo es necesario ejecutar el siguiente comando:
    * > En linux: make clean

## Consideraciones

1. Las instrucciones del documento están probadas en plataformas Linux, aunque debiera funcionar en plataformas Windows, se debe realizar las adecuaciones necesarias para el correcto funcionamiento.