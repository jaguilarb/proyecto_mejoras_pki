#include <ClienteSOAP.h>

ClienteSOAP::ClienteSOAP() {
    inicializar();
}

ClienteSOAP::~ClienteSOAP() {
    liberar();
}

void ClienteSOAP::inicializar() {
    esta_cargado = false;
    n_regimenes = 0;
    n_roles = 0;
    n_obligaciones = 0;
    n_actividades = 0;
}

bool ClienteSOAP::cargar(string nombre_archivo = "") {
    cout << "INFO: Carga de la respuesta del WS IDC [" << nombre_archivo << "].\n";

    if(nombre_archivo.empty()) {
        cout << "ERROR: Carga de la respuesta del WS IDC [" << soap_xml.lastErrorText() << "].\n";

        return false;
    }

    if(!soap_xml.LoadXmlFile(nombre_archivo.c_str())) {
        cout << "ERROR: Carga de la respuesta del WS IDC [" << soap_xml.lastErrorText() << "].\n";

        return false;
    }

    esta_cargado = true;

    return true;
}

bool ClienteSOAP::liberar() {
    if(soap_xml.Clear()) {
        inicializar();

        return true;
    }

    return false;
}

bool ClienteSOAP::estaCargado() {
    return esta_cargado;
}

string ClienteSOAP::regimenes() {
    string regimenes = "";

    CkXml *nodo = soap_xml.SearchForTag(0, "Regimenes");

    while (nodo)
    {
        CkXml *cve_regimen = nodo->FindChild("c_Regimen");
        regimenes = regimenes + string(cve_regimen->content()) + ",";
        CkXml *nodoActual = nodo;
        nodo = soap_xml.SearchForTag(nodoActual,"Regimenes");
        delete nodoActual;
        n_regimenes++;
    }

    return regimenes.substr(0,regimenes.length() - 1);
}

string ClienteSOAP::roles() {
    string roles = "";

    CkXml *nodo = soap_xml.SearchForTag(0, "Roles");

    while (nodo)
    {
        CkXml *cve_rol = nodo->FindChild("c_Rol");
        roles = roles + string(cve_rol->content()) + ",";
        CkXml *nodoActual = nodo;
        nodo = soap_xml.SearchForTag(nodoActual,"Roles");
        delete nodoActual;
        n_roles++;
    }

    return roles.substr(0,roles.length() - 1);
}

string ClienteSOAP::obligaciones() {
    string obligaciones = "";

    CkXml *nodo = soap_xml.SearchForTag(0, "Obligaciones");

    while (nodo)
    {
        CkXml *cve_obligacion = nodo->FindChild("c_Obligacion");
        obligaciones = obligaciones + string(cve_obligacion->content()) + ",";
        CkXml *nodoActual = nodo;
        nodo = soap_xml.SearchForTag(nodoActual,"Obligaciones");
        delete nodoActual;
        n_obligaciones++;
    }

    return obligaciones.substr(0,obligaciones.length() - 1);
}

string ClienteSOAP::actividades() {    
    string actividades = "";

    CkXml *nodo = soap_xml.SearchForTag(0, "Actividades");

    while (nodo)
    {
        CkXml *cve_actividad = nodo->FindChild("c_Actividad");
        actividades = actividades + string(cve_actividad->content()) + ",";
        CkXml *nodoActual = nodo;
        nodo = soap_xml.SearchForTag(nodoActual,"Actividades");
        delete nodoActual;
        n_actividades++;
    }

    return actividades.substr(0,actividades.length() - 1);
}

int ClienteSOAP::numRegimenes() {
    return n_regimenes;
}

int ClienteSOAP::numRoles() {
    return n_roles;
}

int ClienteSOAP::numObligaciones() {
    return n_obligaciones;
}

int ClienteSOAP::numActividades() {
    return n_actividades;
}