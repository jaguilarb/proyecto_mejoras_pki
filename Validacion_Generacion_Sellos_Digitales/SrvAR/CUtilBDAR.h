/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza las consultas en DARIO                                                 ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Gudelia Hern�ndez Molina        GHM                                                    ###
  ###  FECHA DE INICIO:       Jueves 03, enero del 2006                                                          ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
*/
/*######################################################################################################################
   VERSION:
       V.1.00      (20060103 -         ) GHM: Primera Versi�n
          .01
             .02   (20141001 -         ) JAB: Agrega el m�todo que ejecuta el procedimiento almacenado para validar si un
                                             regimen puede generar sellos digitales. Se eliminan m�todos ya no utilizados
                                             para la validaci�n
   CAMBIOS:
######################################################################################################################*/

#ifndef _CUTILBDAR_H_
#define _CUTILBDAR_H_
static const char* _CUTILBDAR_H_VERSION_ ATR_USED = "SrvAR @(#)"\
"DSIC09041AR_ 2007-12-13 CUtilBDAR.h 1.1.1/3";

//#VERSION: 1.1.1

#include <string.h>       //Manejo de Strings

#include <Sgi_BD.h>
#include <DefDatos.h>
using namespace std;

// ******************************  DEFINICIONES DE LA CLASE  *******************************
class CUtilBDAR
{
   public:
      //DEFINICION DE VARIABLES Y METODOS PUBLICOS
      string   m_MsgDesc;        //Describe Error,Situacion, etc
     
      CUtilBDAR();
      virtual ~CUtilBDAR();
      
      //FUNCIONES DE INICIALIZACION
      bool Inicializa(CBD* varCnxBDAR);
      bool ValInic(const char *storeProc);
      //FUNCIONES PARA EL MANEJO DE ERRORES
      bool TrataErrorProc(int itpoMsg, const char* sCausa);
      //FUNCIONES QUE OBTIENEN DATOS DE AR
      bool ObtSitFiscal(int cveSitFis, string * DescSF, char* tpoCerVal, char* valDomic );
      bool ObtSitDomic(int cveSitDom, string * DescSD, char* tpoCerVal );
      bool ObtFEAacxRFC(const char* RFCBsq, int* numCertAct, char* rfc, char* numSerie); 
      bool tieneFEAact(char* RFCBsq);
      int  getNumCerts(const char* rfc, int tipo, char estado);
      bool revSituacionFyD(uint16 tipoCert, int cveSitFis, int cveSitDom);
      int  valGenTipCDxAGC(const char* agc, int tipo);
      bool getEdoAgCyCert(const char* agc, char* edoAgC, char* edoCert);
      int  getEdoyTipCert(const char* numSerie, char* edoCert, int *tipCert);
      int  valLlaveUnica(const char* RFCBsq, const char* digPKmd5, const char* digPKsha1 );
      int valGenSellos(int cve_regimen, const char* cve_roles, const char* cve_obligs, const char* cve_activs, string* mensaje);
      //>+ ERGL (070116)
      bool getFecVigFin(const char *numSerie, char *fecha);
      //FUNCIONES DE AFECTACION EN LA BD
      int  ApartaLlave(const char* numOper, const char* rfc, const char* dig_md5, const char* dig_sha1, int* oper_sec);
      bool delLlaveApartada(const char* numOper, int secuencia );
      int  RegOperDetalleBDAR(int cveProc, char* numOp, int numError = 0);
     // bool RegOperDetalleBDAR(int cveProc, char* numOp);
      bool RegErrorOpDetBDAR( char* numOp, int numError );

      int  getNivelAGC(const char *agcCve);
      bool RegistraRFC( char * cRFCOrig , char * cRFC );
      int  verificaVigencia(const char* numSerie, int *vigente);
      int  RFCBloqueado(const char* RFCBsq );

      bool getCURP(const char *rfc, string* sCURP );
 
   private:
      //DEFINICION DE VARIABLES Y METODOS PRIVADOS
   protected:
      //DEFINICION DE VARIABLES Y METODOS PROTEGIDOS
      CBD*     m_CnxBDAR;              //Apuntador a la conexi�n de la AR
      // int      m_numError;          //N�mero de Error
      char     m_tipoError;            //Tipo de error 'I'nterno � 'N'egocio
      bool     m_inicializado;         //Indica si ya se tiene el apuntador a la conexi�n
      
      virtual const char * getNombreOperacion() {return "UTIL_BDAR"; }
      
};

extern CBitacora* Bitacora;

#endif