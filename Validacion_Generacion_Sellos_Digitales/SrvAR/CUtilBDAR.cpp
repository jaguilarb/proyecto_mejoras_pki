static const char* _CUTILBDAR_CPP_VERSION_ ATR_USED = "@(#) SrvAR ( L : DSIC10392AR_ : CUtilBDAR.cpp : 1.1.2 : 4 : 28/09/10)";

//#VERSION: 1.1.1
/*######################################################################################################################
  ###  PROYECTO:              PKI-SAT                                                                                ###
  ###  MODULO:                Clase que realiza las consultas, validaciones y otras funciones en la BD de la AR      ###
  ###                                                                                                                ###
  ###  DESARROLLADORES:       Gudelia Hern�ndez Molina        GHM                                                    ###
  ###  FECHA DE INICIO:       Miercoles 25 de enero del 2006                                                         ###
  ###                                                                                                                ###
  ######################################################################################################################
         1         2         3         4         5         6         7         8         9        10        11        12
1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*######################################################################################################################
   VERSION:  
      V.1.00      (20060125 -         ) GHM: Primera Versi�n
                                             Esta clase se gener� para realizar las validaciones en DARIO.
      V.1.01      (20081211 -         ) GHM: Agregar las consultas a la BD sobre la validaci�n de CRM para SDG
            .01                              - Verificar que la CveRegimen exista en la BD y obtener su operador
            .02   (20141001 -         ) JAB: Agrega el m�todo que ejecuta el procedimiento almacenado para validar si un
                                             regimen puede generar sellos digitales. Se eliminan m�todos ya no utilizados
                                             para la validaci�n

   CAMBIOS:
######################################################################################################################*/
#include <CUtilBDAR.h>

//######################################################################################################################
//Declaraci�n de los CONSTRUCTORES
CUtilBDAR::CUtilBDAR()
{
   //Inicializacion de variables
   //m_numError = 0;
   m_CnxBDAR = NULL;
   m_inicializado = false;
}

//######################################################################################################################
//Declaraci�n del Destructor
CUtilBDAR::~CUtilBDAR()
{
   m_CnxBDAR = NULL;
   //Libera los apuntadores que se hayan creado
}

//FUNCIONES DE INICIALIZACION
//######################################################################################################################
//Inicializa la conexi�n del Certificado
bool CUtilBDAR::Inicializa(CBD* varCnxBDAR)
{
   m_CnxBDAR = varCnxBDAR;
   return ( m_inicializado = (m_CnxBDAR != NULL) );
}

//######################################################################################################################
bool CUtilBDAR::ValInic(const char *storeProc)
{
   if (!m_inicializado)
   {
      m_MsgDesc = "UtilBDAR(P): Error interno,  solicite apoyo al inform�tico local ";
      Bitacora->escribePV(BIT_ERROR, "UtilBDAR(P): No est� inicializada la conexi�n a base de datos (%s)",
                          storeProc);
   }
   return m_inicializado;
}

//FUNCIONES QUE OBTIENEN DATOS DE AR
//######################################################################################################################
bool CUtilBDAR::ObtSitFiscal(int cveSitFis, string * DescSF, char* tpoCerVal, char* valDomic )
{
   //GHM (000218): Se modific� para poder tener simultaneamente la version anterior y la nueva con CRM
   //- if ( !(m_CnxBDAR->consultaReg("SELECT sitfis_desc, tipcerval, valdomic FROM cat_sit_fis "
   if ( !(m_CnxBDAR->consultaReg("SELECT sitfis_desc, tipcerval, valdomic FROM cat_sit_fisCRM"
                                 "WHERE sitfis_cve = %i", cveSitFis)) )
   {
      Bitacora->escribePV(BIT_ERROR, "UtilBDAR(P): Al extraer la descripci�n de la Situaci�n Fiscal del RFC (%s)",
                                     cveSitFis);
      return TrataErrorProc(MSGDESC_ICS, "al obtener la Situaci�n Fiscal del Contribuyente");
   }
   if ( !(m_CnxBDAR->getValores("sbb", DescSF, tpoCerVal, valDomic)) )
   {
      Bitacora->escribe(BIT_ERROR, "UtilBDAR(P): Al obtener la descripci�n de la Situaci�n Fiscal");
      return TrataErrorProc(MSGDESC_ICS, "al obtener la Situaci�n Fiscal del Contribuyente");
   }
   if (DescSF->length() == 0)
   {
       m_tipoError = 'O';
       Bitacora->escribePV(BIT_ERROR, "UtilBDAR(P): Al obtener la descripci�n de la Situaci�n Fiscal (%s)", cveSitFis);
       m_MsgDesc = "No se cuenta con la descripci�n de la Situaci�n Fiscal, solicite la verificaci�n";
       return false;
   }
   return true;
}

//######################################################################################################################
bool CUtilBDAR::ObtSitDomic(int cveSitDom, string * DescSD, char* tpoCerVal )
{
   //GHM (000218): Se modific� para poder tener simultaneamente la version anterior y la nueva con CRM
   //- if ( !(m_CnxBDAR->consultaReg("SELECT sitdom_desc, tipCerVal FROM cat_sit_domic "
   if ( !(m_CnxBDAR->consultaReg("SELECT sitdom_desc, tipCerVal FROM cat_sit_domicCRM "
                                 "WHERE sitdom_cve = %i", cveSitDom)) )
   {
      Bitacora->escribe(BIT_ERROR, "UtilBDAR(P): Al extraer la descripci�n de la Situaci�n de Domicilio");
      return TrataErrorProc(MSGDESC_ICS, "al obtener la Situaci�n de Domicilio del Contribuyente");
   }
   if ( !(m_CnxBDAR->getValores("sb", DescSD, tpoCerVal)) )
   {
      Bitacora->escribe(BIT_ERROR, "UtilBDAR(P): Al obtener la descripci�n de la Situaci�n de Domicilio");
      return TrataErrorProc(MSGDESC_ICS, "al obtener la Situaci�n de Domicilio del Contribuyente");
   }
   if (DescSD->length() == 0)
   {
       m_tipoError= 'O';
       Bitacora->escribePV(BIT_ERROR, "UtilBDAR(P): Al obtener la descripci�n de la Situaci�n de Domicilio (%s)",
                                      cveSitDom);
       m_MsgDesc = "No se cuenta con la descripci�n de la Situaci�n de Domicilio, solicite la verificaci�n";
       return false;
   }
   return true;
}

//######################################################################################################################
bool CUtilBDAR::ObtFEAacxRFC(const char* RFCBsq, int* numCertAct, char* rfc, char* numSerie)
{
   char numCert[5]; 
   if (!ValInic("sp_getFEAacxRFC"))
      return false;
   if ( !m_CnxBDAR->ejecutaSP("sp_getFEAacxRFC", "'%s'", RFCBsq) )
   {
      Bitacora->escribePV(BIT_ERROR, "UtilBDAR(P): Al consultar sp_getFEAacxRFC del RFC (%s)", rfc);
      return TrataErrorProc(MSGDESC_ICS, "al consultar si se cuenta con certificado activo de FIEL");
   }
   if ( !m_CnxBDAR->getValores("ibb", numCertAct, rfc, numSerie) )
   {
      Bitacora->escribePV(BIT_ERROR, "UtilBDAR(P): Al obtener sp_getFEAacxRFC del RFC (%s)", rfc);
      return TrataErrorProc(MSGDESC_ICS, "al obtener si se cuenta con certificado activo de FIEL");
   }
   if (*numCertAct > 0)
   {
      sprintf(numCert, "%i", *numCertAct);
      Bitacora->escribePV(BIT_INFO, "UtilBDAR(P): Se encontr� que cuenta con (%i) certificados activos "
                                     "el RFC (%s) y el primero tiene n�mero de serie: %s ",
                                     *numCertAct, rfc, numSerie);
       m_MsgDesc = "Se encontr� que el RFC: " + string(rfc) + " cuenta con " + string(numCert) +
                   "\ncertificados activos y el primero tiene n�mero de serie: " + string(numSerie) + 
                   "\nDebe primero revocarlos";
      return true;
   }
   return false;
}

//######################################################################################################################
bool CUtilBDAR::tieneFEAact(char* RFCBsq)
{
   int numCertAct = 0;
   char rfcUtilCert[14];
   char numSerieCert[22];
   rfcUtilCert[0] = 0;
   return (ObtFEAacxRFC( RFCBsq, &numCertAct, rfcUtilCert, numSerieCert));
}
//######################################################################################################################
int CUtilBDAR::getNumCerts(const char* rfc, int tipo, char estado)
{
   //GHM:060201 Pendiente, se debe contemplar la construcci�n de un store procedure que haga esta tarea
   //           utilizando los RFC asociados y banderas del tipo de certificados 
   int  cuantos;
   char cond[40];
   
   string sql("SELECT count(*) FROM certificado WHERE rfc = '%s'");
   if (tipo)
   {
      sprintf(cond, " and tipcer_cve = %d", tipo);
      sql += cond;
   }
   if (estado)
   {
      sprintf(cond, " and edo_cer = '%c'", estado);
      sql += cond; 
   }
         
   if (m_CnxBDAR->consultaReg(sql.c_str(), rfc) &&
       m_CnxBDAR->getValores("i", &cuantos))
      return cuantos;

   return -1;
}
//######################################################################################################################
bool CUtilBDAR::revSituacionFyD(uint16 tipoCert, int cveSitFis, int cveSitDom)
{
   int  tipCerVal   = 0; 
   char valDomic    = 'N';
   if ( !ValInic("revSituacionFyD") )
      return false;
   //GHM (000218): Se modific� para poder tener simultaneamente la version anterior y la nueva con CRM
   //- if ( !(m_CnxBDAR->consultaReg("SELECT tipCerVal, valDomic FROM cat_sit_fis WHERE sitfis_cve = %d", cveSitFis)) )
   if ( !(m_CnxBDAR->consultaReg("SELECT tipCerVal, valDomic FROM cat_sit_fisCRM WHERE sitfis_cve = %d", cveSitFis)) )
   {
       Bitacora->escribePV(BIT_ERROR, "UtilBDAR(P): Al validar la Situaci�n Fiscal (%i) del tipo de certificado (%i)",
                           cveSitFis, tipoCert);
       return TrataErrorProc(MSGDESC_CS, "No se pudo validar la Situaci�n Fiscal del Contribuyente");
   }
   if ( !(m_CnxBDAR->getValores("ic", &tipCerVal, &valDomic)) )
   {
      Bitacora->escribePV(BIT_ERROR, "UtilBDAR(P): Al obtener los datos de la Situaci�n Fiscal (%i)", cveSitFis);
      return TrataErrorProc(MSGDESC_ICS, "Al obtener la Situaci�n Fiscal del Contribuyente");
   }
   if ((tipCerVal & tipoCert) == 0)
   {
      Bitacora->escribePV(BIT_ERROR, "UtilBDAR(P): La Situaci�n Fiscal (%i), no permite generar certificados del "
                                     "tipo solicitado (%i)", cveSitFis, tipoCert);
      m_MsgDesc = "La Situaci�n Fiscal no est� autorizada para generar el tipo de certificado solicitado.";  
      return false;
   }
   if (valDomic == 'S')
   {
      //GHM (000218): Se modific� para poder tener simultaneamente la version anterior y la nueva con CRM
      //- if ( !(m_CnxBDAR->consultaReg("SELECT tipCerVal FROM cat_sit_domic WHERE SitDom_cve = %d", cveSitDom)) )
      if ( !(m_CnxBDAR->consultaReg("SELECT tipCerVal FROM cat_sit_domicCRM WHERE SitDom_cve = %d", cveSitDom)) )
      {
         Bitacora->escribePV(BIT_ERROR, "UtilBDAR(P): Al validar la Situaci�n de Domicilio (%i) del tipo de "
                                        " certificado (%i)", cveSitDom, tipoCert);
          return TrataErrorProc(MSGDESC_CS, "No se pudo validar la Situaci�n de Domicilio del Contribuyente");
      }
      if ( !(m_CnxBDAR->getValores("i", &tipCerVal)) )
      {
         Bitacora->escribe(BIT_ERROR, "UtilBDAR(P): Al obtener los datos de la Situaci�n de Domicilio");
         return TrataErrorProc(MSGDESC_ICS, "Al obtener la Situaci�n de Domicilio del Contribuyente");
      }
      if ((tipCerVal & tipoCert) == 0)
      {
         Bitacora->escribePV(BIT_ERROR, "UtilBDAR(P): La Situaci�n de Domicilio (%i), no permite generar certificados "
                                        "del tipo solicitado (%i)", cveSitDom, tipoCert);
         m_MsgDesc = "La Situaci�n de Domicilio no est� autorizada para generar el tipo de certificado solicitado";  
         return false;
      }
   }
   //else No es necesario evaluar la situaci�n de domicilio
   //else // No es una situaci�n v�lida de Domicilio
  //  return false;
   return true;
    
}

//######################################################################################################################

//######################################################################################################################
int CUtilBDAR::valGenTipCDxAGC(const char* agc, int tipo)
{
   int cuantos;

   if (m_CnxBDAR->consultaReg("SELECT count(*) FROM AGENTE a, NIVEL_TIP_CER n WHERE a.agc_cve = '%s' AND "
                              "a.nivel_cve = n.nivel_cve AND n.tipcer_cve = %d", agc, tipo) &&
       m_CnxBDAR->getValores("i", &cuantos))
      return cuantos; 

   return -1;
}

//######################################################################################################################
bool CUtilBDAR::getEdoAgCyCert(const char* agc, char* edoAgC, char* edoCert)
{
   if ( m_CnxBDAR->consultaReg("SELECT  a.estado, c.edo_cer FROM AGENTE a, CERTIFICADO c "
                               "  WHERE a.agc_cve = '%s' AND a.no_serie = c.no_serie", agc) &&
       m_CnxBDAR->getValores("cc", edoAgC, edoCert) )
      return true; 
   Bitacora ->escribePV(BIT_ERROR, "CUtilBDAR(I): No se obtuvo correctamente los datos");
   return false;
}

//######################################################################################################################
/* Retorna:   -1      Error interno
               0      Si no hay ningun registro de esa llave
               1      Se ejecuto correctamente
*/
int CUtilBDAR::getEdoyTipCert(const char* numSerie, char* edoCert, int *tipCert)
{
   if ( !(m_CnxBDAR->consultaReg("SELECT edo_cer, tipcer_cve FROM Certificado WHERE no_serie = '%s';",numSerie)) )
   {
      Bitacora->escribe(BIT_ERROR, "CUtilBDAR(I): Al consultar con la funci�n getEdoyTipCert" );
      return -1;
   }
   if ( !(m_CnxBDAR->getValores("ci", edoCert, tipCert)) ) //Puede no existir datos
   {
      //GHM:080407 Se expresa mal el error
      //Bitacora->escribe(BIT_ERROR, "CUtilBDAR(I): Al extraer los datos obtenidos del store sp_valLlaveUnica");
      Bitacora->escribePV(BIT_INFO, "CUtilBDAR(I): No se obtuvo datos del certificado con n�mero de serie '%s' utilizando el SP getEdoyTipCert",
                                    numSerie);
      return 0; 
   }
   return 1;
}
//######################################################################################################################
/* Retorna:   -1      Error interno
               0      Si no hay ningun registro de esa llave  
               n > 0  Clave de error de negocio
               La descripci�n del error de negocio se puede ver en m_MsgDesc
*/
int CUtilBDAR::valLlaveUnica(const char* RFCBsq, const char* digPKmd5, const char* digPKsha1 )
{
   int cveErrorNeg;
   //Valida el n�mero de serie en la BD
   if ( !(m_CnxBDAR->ejecutaSP("sp_valLlaveUnica", "'%s','%s','%s'", RFCBsq, digPKmd5, digPKsha1)) )
   {
      Bitacora->escribe(BIT_ERROR, "CUtilBDAR(I): Al ejecutar el store sp_valLlaveUnica" );
      return -1;
   }
   //Verifica el resultado obtenido
   if ( !(m_CnxBDAR->getValores("is", &cveErrorNeg, &m_MsgDesc)) )
   {
      Bitacora->escribe(BIT_ERROR, "CUtilBDAR(I): Al extraer los datos obtenidos del store sp_valLlaveUnica");
      return -1;
   }
   if (cveErrorNeg > 0) 
      Bitacora->escribePV(BIT_INFO, "CUtilBDAR(N): %s", m_MsgDesc.c_str());
   return cveErrorNeg;
}
//>>> ERGL (070116)
bool CUtilBDAR::getFecVigFin(const char *numSerie, char *fecha)
{
   if( !m_CnxBDAR->consultaReg("SELECT vig_fin FROM CERTIFICADO WHERE no_serie = '%s'", numSerie) )
   {
      Bitacora->escribe(BIT_ERROR, "CUTILBDAR(I): Al recuperar la fecha de vigencia o caducidad.");
      return false;
   }
   if( !m_CnxBDAR->getValores("b", fecha) )
   {
      Bitacora->escribe(BIT_ERROR, "CUTILBDAR(I): Al extraer los datos de la consulta de fecha de vigencia o caducidad.");
      return false;
   }
   return true;
}
//<<<

//######################################################################################################################
/**
 * Ejecuta el procedimiento almacenado que valida si un regimen puede generar sellos digitales
 *
 * @param cve_regimen La clave del regimen a validar
 * @param cve_roles   La lista de claves de roles a validar junto con el regimen
 * @param cve_obligs  La lista de claves de obligaciones a validar junto con el regimen
 * @param cve_activs  La lista de claves de actividades a validar junto con el regimen
 * @param mensaje     El mensaje resultado de la ejecuci�n del procedimiento almacenado
 *
 * @return  -1  Error interno
 *          0   No puede generar sellos
 *          1   Puede generar sellos
 */
int CUtilBDAR::valGenSellos(int cve_regimen, const char* cve_roles, const char* cve_obligs, const char* cve_activs, string* mensaje)
{
  int validacion;

  // Si no hay conexi�n a la base de datos termina
  if (!ValInic("sp_valida_gen_sd"))
  {
    return -1;
  }

  // Ejecuta el procedimiento almacenado para validar si el regimen puede generar sellos digitales
  if (!(m_CnxBDAR->ejecutaSP("sp_valida_gen_sd", "%d,'SET{%s}','SET{%s}','SET{%s}'", cve_regimen, cve_roles, cve_obligs, cve_activs)))
  {
    Bitacora->escribe(BIT_ERROR, "CUtilBDAR(I): Al ejecutar el store sp_valida_gen_sd");

    return -1;
  }
  
  // Obtiene el resultado
  if (!(m_CnxBDAR->getValores("is", &validacion, mensaje)))
  {
    Bitacora->escribe(BIT_ERROR, "CUtilBDAR(I): Al extraer los datos obtenidos del store sp_valida_gen_sd");
    
    return -1;
  }
  
  return validacion;
}
//######################################################################################################################


//FUNCIONES DE AFECTACION EN LA BD
//######################################################################################################################
int CUtilBDAR::RegOperDetalleBDAR(int cveProc, char* numOp, int numError)
{
   int consecutivo, errorSP;
   if ( !m_CnxBDAR->ejecutaSP("sp_regOperDet", "'%s', %i, %i", numOp, cveProc, numError) )
   {
      Bitacora->escribePV(BIT_ERROR, "CUtilBDAR(I): No se registr� con RegOperDetalleBDAR cveProc(%i), numOp(%s), "
                                     " error(%i)", cveProc, numOp, numError);
      return -1;
   }
   if (!m_CnxBDAR->getValores("ii", &errorSP, &consecutivo) )
   {
      Bitacora->escribe(BIT_ERROR, "CUtilBDAR(I): No se obtuvo correctamente los datos de RegOperDetalleBDAR");
      return -1;
   }
   if (errorSP != 0)
   {
      Bitacora->escribePV(BIT_ERROR, "CUtilBDAR(I): Se present� error en RegOperDetalleBDAR ( %d ).",errorSP);
      return -1;
   }
   return consecutivo;
}

//######################################################################################################################
/* Retorna:   -1      Error interno
               0      Si no hay ningun registro de esa llave
               n > 0  Clave de error de negocio
               La descripci�n del error de negocio se puede ver en m_MsgDesc
*/
bool CUtilBDAR::delLlaveApartada(const char* numOper, int secuencia )
{
   bool Ok;
   //Valida el n�mero de serie en la BD
   if( !(Ok = m_CnxBDAR->ejecutaOper("DELETE FROM llave_apartada WHERE SolOper_cve = '%s' and oper_sec = %i",
                              numOper, secuencia)) )
      Bitacora->escribePV(BIT_ERROR, "CUtilBDAR(I): No se pudo eliminar la llave apartada correspondiente al "
                                     "N�mero Operaci�n: %s y secuencia: %i", numOper, secuencia );
   return Ok;
}

//######################################################################################################################
bool CUtilBDAR::RegErrorOpDetBDAR( char* numOp, int numError )
{
  return m_CnxBDAR->ejecutaSP("sp_regError", "'%s', %i", numOp, numError);
}
//######################################################################################################################
int CUtilBDAR::ApartaLlave(const char* numOper, const char* rfc, const char* dig_md5, 
                           const char* dig_sha1, int* oper_sec)
{
   return m_CnxBDAR->ejecutaSP("sp_regLlvApart", "'%s','%s','%s','%s'", numOper, rfc, dig_md5, dig_sha1) &&
          m_CnxBDAR->getValores("i", oper_sec);
}
//######################################################################################################################
//FUNCIONES PARA EL MANEJO DE ERRORES
//######################################################################################################################
bool CUtilBDAR::TrataErrorProc(int itpoMsg, const char* sCausa )
{
   //indicador de tipo de error (Interno)
   m_tipoError = 'I';
   char inicio[] = "Se present� un error ";
   char soluc[]  = ", \n favor de intentar nuevamente, si se vuelve a presentar el error solicite "
                   "apoyo al inform�tico local";
   switch (itpoMsg)
   {
      case MSGDESC_ICS:
         m_MsgDesc = string(inicio) + string(sCausa) + string(soluc);
         break;
      case MSGDESC_CS:
         m_MsgDesc = string(sCausa) + string(soluc);
         break;
      case MSGDESC_VACIO :
         m_MsgDesc = "";
         break;
   }
   return false;
}

//#############################################################################################################
/* Obtiene el num. de nivel del AgC de la DB, de la tabla AGENTE, campo: nivel_cve
   (in)  agcCve   : es la clave del Agente Certificador i.e. COperacion::m_agcCve
   OjO MELM RETORNA 0 si hay error � regresa en la variable error el campo 'nivel_cve' de la tabla 'AGENTE'
*/
int CUtilBDAR::getNivelAGC(const char *agcCve)
{
   int error = 0;
   if ( !m_CnxBDAR->consultaReg("SELECT nivel_cve FROM Agente WHERE AgC_cve = '%s'", agcCve) || 
        !m_CnxBDAR->getValores("i", &error ) ) 
   {
      error = 0; //error = ERR_GET_NIVEL_AGC;
      Bitacora->escribePV(BIT_ERROR, "Error en getNivelAgC(agcCve=%s)", agcCve );
   }
   return error;
}

//#############################################################################################################
bool CUtilBDAR::RegistraRFC( char * cRFCOrig , char * cRFC )
{
    char  cRFCAux[15];
    char  cRFCOAux[15];
    string  sRFCA_aux;

    if( strlen(cRFC) == 12)
       sprintf(cRFCAux ," %s",cRFC);
    else 
       sprintf(cRFCAux ,"%s",cRFC);
    
    if( strlen(cRFCOrig) == 12)
       sprintf(cRFCOAux ," %s",cRFCOrig);
    else
       sprintf(cRFCOAux ,"%s",cRFCOrig);
        
    
    if( m_CnxBDAR->consultaReg("SELECT rfc FROM RFC_ASOCIADO WHERE rfc_orig ='%s' and rfc ='%s'", cRFCOAux ,cRFCAux) )
    {
       
       if(m_CnxBDAR->getValores("s", &sRFCA_aux ))
       {
          if(!strcmp(sRFCA_aux.c_str(), cRFCAux))
             return true;
       }
       else
       {
          Bitacora->escribePV(BIT_ERROR, "No se obtuvieron los datos de la consulta");   
       }
       
       if( !m_CnxBDAR->ejecutaOper("INSERT INTO RFC_ASOCIADO  VALUES ( '%s' , '%s');", cRFCOAux , cRFCAux  ) )
       {
         Bitacora->escribePV(BIT_ERROR, "No se registraron los datos ( %s , %s ) en la tabla RFC_ASOCIADO", cRFCOAux ,cRFCAux);
         return false;
       }
       else return true;             
    }
    Bitacora->escribePV(BIT_ERROR, "Error  en consulta ( %s , %s ) en la tabla RFC_ASOCIADO", cRFCOAux ,cRFCAux);
    return false;
   
}

//#############################################################################################################
// Valida la Vigencia del num_serie en la DB de la AR
/* Retorna:   -1      Error interno
               0      Si no hay ningun registro de esa llave
               1      Se ejecuto correctamente
*/
int CUtilBDAR::verificaVigencia(const char* numSerie, int *vigente)
{
   if ( !( m_CnxBDAR->ejecutaSP("sp_valFecCert", "'%s'", numSerie) ) )
   {
      Bitacora->escribePV(BIT_ERROR, "CUtilBDAR(I): no pudo ejecutarse el sp_valFecCert(%s) en la DB de la AR", numSerie );
      return -1;
   }
   if ( !(m_CnxBDAR->getValores("i", vigente)) ) // Puede no existir datos
   {
      Bitacora->escribePV(BIT_ERROR, "CUtilBDAR(I): Al extraer los datos obtenidos del store sp_valFecCert(%s)", numSerie );
      return 0;
   }
   return 1;
}

//######################################################################################################################
/* Retorna:   -1      Error interno
               0      Si el rfc no esta bloqueado
               1      Si el rfc esta Bloqueado
*/
int CUtilBDAR::RFCBloqueado(const char* RFCBsq )
{
   int bloqueado;
   
   if ( !(m_CnxBDAR->ejecutaSP("sp_verif_bloq", "'%s'", RFCBsq )) )
   {
      Bitacora->escribe(BIT_ERROR, "CUtilBDAR(I): Al ejecutar el store sp_verif_bloq" );
      return -1;
   }
   if ( !(m_CnxBDAR->getValores("i", &bloqueado)) )
   {
      Bitacora->escribe(BIT_ERROR, "CUtilBDAR(I): Al extraer los datos obtenidos del store sp_verif_bloq");
      return -1;
   }
   return bloqueado;
}

//#############################################################################################################
/* Obtiene el CURP del  RFC daddo
   rfc   : Es el RFC del contribuyente que se va buscar la curp
*/
bool  CUtilBDAR::getCURP(const char *rfc, string* sCURP )
{
   bool  resp = false;
   int error;
   if ( !m_CnxBDAR->consultaReg("SELECT curp FROM certificado  WHERE rfc = '%s' and edo_cer='A' and tipcer_cve=1 ", rfc) ||
        !m_CnxBDAR->getValores("i", &error ) )
      Bitacora->escribePV(BIT_ERROR, "Error en getCURP(rfc=%s)", rfc );
   else
   {
      if ( !(m_CnxBDAR->getValores("s", sCURP)) )
      {
         Bitacora->escribe(BIT_ERROR, "UtilBDAR(P): Al obtener la CURP del RFC solicitado");
         return TrataErrorProc(MSGDESC_ICS, "al obtener la CURP del RFC solicitado");
      }
      resp = true;
   }
   return resp;
}