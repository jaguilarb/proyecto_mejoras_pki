CREATE TABLE SEG_ARA.SEGC_TIP_CER
(
  TIPCERCVE   NUMBER(5) CONSTRAINT VNN_CTIPCER_CVE NOT NULL,
  TIPCERDESC  VARCHAR2(50 BYTE) CONSTRAINT VNN_CTIPCER_DES NOT NULL
)
TABLESPACE SEGLC_ARA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOCOMPRESS 
/

CREATE TABLE SEG_ARA.SEGT_CER_AC
(
  EMISORCVE  NUMBER(5) CONSTRAINT VNN_CERAC_CVE NOT NULL,
  NOSERIE    CHAR(20 BYTE) CONSTRAINT VNN_CERAC_NOSER NOT NULL,
  DIGNOM     CHAR(24 BYTE) CONSTRAINT VNN_CERAC_DNOM NOT NULL,
  DIGPBK     CHAR(24 BYTE) CONSTRAINT VNN_CERAC_DPBK NOT NULL
)
TABLESPACE SEGLT_ARA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOCOMPRESS 
/


CREATE TABLE SEG_ARA.SEGT_CERTIFICADO
(
  NOSERIE    CHAR(20 BYTE) CONSTRAINT VNN_CER_NOSER NOT NULL,
  TIPCERCVE  NUMBER(5) CONSTRAINT VNN_CER_TIPCVE NOT NULL,
  EDOCER     CHAR(1 BYTE) CONSTRAINT VNN_CER_EDOCER NOT NULL,
  RFC        CHAR(13 BYTE) CONSTRAINT VNN_CER_RFC NOT NULL,
  CURP       CHAR(18 BYTE),
  VIGINI     DATE CONSTRAINT VNN_CER_VIGINI     NOT NULL,
  VIGFIN     DATE CONSTRAINT VNN_CER_VIGFIN     NOT NULL
)
TABLESPACE SEGLT_ARA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOCOMPRESS 
/



CREATE TABLE SEG_ARA.SEGT_RANGOS_AC
(
  EMISORCVE  NUMBER(5) CONSTRAINT VNN_RANGAC_CVE NOT NULL,
  SEC        NUMBER(5) CONSTRAINT VNN_RANGAC_SEC NOT NULL,
  ACID       NUMBER(10) CONSTRAINT VNN_RANGAC_ACID NOT NULL,
  RANGOINI   NUMBER(10) CONSTRAINT VNN_RANGAC_RINI NOT NULL,
  RANGOFIN   NUMBER(10) CONSTRAINT VNN_RANGAC_RFIN NOT NULL
)
TABLESPACE SEGLT_ARA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOCOMPRESS 
/


--
-- UK_SEGTRANGOSAC_EMISORCVE_SEC  (Index) 
--
CREATE UNIQUE INDEX SEG_ARA.UK_SEGTRANGOSAC_EMISORCVE_SEC ON SEG_ARA.SEGT_RANGOS_AC
(EMISORCVE, SEC)
TABLESPACE SEGLI_ARA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
/


--
-- UK_SEGTCERTIFICADO_NOSERIE  (Index) 
--
CREATE UNIQUE INDEX SEG_ARA.UK_SEGTCERTIFICADO_NOSERIE ON SEG_ARA.SEGT_CERTIFICADO
(NOSERIE)
TABLESPACE SEGLI_ARA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
/


--
-- UK_SEGTCERAC_EMISORCVE  (Index) 
--
CREATE UNIQUE INDEX SEG_ARA.UK_SEGTCERAC_EMISORCVE ON SEG_ARA.SEGT_CER_AC
(EMISORCVE)
TABLESPACE SEGLI_ARA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
/


--
-- UK_SEGCTIPCER_TIPCERCVE  (Index) 
--
CREATE UNIQUE INDEX SEG_ARA.UK_SEGCTIPCER_TIPCERCVE ON SEG_ARA.SEGC_TIP_CER
(TIPCERCVE)
TABLESPACE SEGLI_ARA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
/


-- 
-- Non Foreign Key Constraints for Table SEGC_TIP_CER 
-- 
ALTER TABLE SEG_ARA.SEGC_TIP_CER ADD (
  CONSTRAINT VPK_CTIPCER
  PRIMARY KEY
  (TIPCERCVE)
  USING INDEX SEG_ARA.UK_SEGCTIPCER_TIPCERCVE
  ENABLE VALIDATE)
/
-- 
-- Non Foreign Key Constraints for Table SEGT_CER_AC 
-- 
ALTER TABLE SEG_ARA.SEGT_CER_AC ADD (
  CONSTRAINT VPK_CEMISOR
  PRIMARY KEY
  (EMISORCVE)
  USING INDEX SEG_ARA.UK_SEGTCERAC_EMISORCVE
  ENABLE VALIDATE)
/


-- 
-- Non Foreign Key Constraints for Table SEGT_CERTIFICADO 
-- 
ALTER TABLE SEG_ARA.SEGT_CERTIFICADO ADD (
  CONSTRAINT VCH_CER_EDOCER
  CHECK ("EDOCER"='A' OR "EDOCER"='S' OR "EDOCER"='R' OR "EDOCER"='C')
  ENABLE VALIDATE)
/

ALTER TABLE SEG_ARA.SEGT_CERTIFICADO ADD (
  CONSTRAINT VPK_CER_NOSER
  PRIMARY KEY
  (NOSERIE)
  USING INDEX SEG_ARA.UK_SEGTCERTIFICADO_NOSERIE
  ENABLE VALIDATE)
/


-- 
-- Non Foreign Key Constraints for Table SEGT_RANGOS_AC 
-- 
ALTER TABLE SEG_ARA.SEGT_RANGOS_AC ADD (
  CONSTRAINT VPK_RNG
  PRIMARY KEY
  (EMISORCVE, SEC)
  USING INDEX SEG_ARA.UK_SEGTRANGOSAC_EMISORCVE_SEC
  ENABLE VALIDATE)
/


-- 
-- Foreign Key Constraints for Table SEGT_CERTIFICADO 
-- 
ALTER TABLE SEG_ARA.SEGT_CERTIFICADO ADD (
  CONSTRAINT FK_TIP_CER_CERTIFICADO 
  FOREIGN KEY (TIPCERCVE) 
  REFERENCES SEG_ARA.SEGC_TIP_CER (TIPCERCVE)
  ENABLE VALIDATE)
/
-- 
-- Foreign Key Constraints for Table SEGT_RANGOS_AC 
-- 
ALTER TABLE SEG_ARA.SEGT_RANGOS_AC ADD (
  CONSTRAINT FK_CER_AC_RANGOS_AC 
  FOREIGN KEY (EMISORCVE) 
  REFERENCES SEG_ARA.SEGT_CER_AC (EMISORCVE)
  ENABLE VALIDATE)
/
