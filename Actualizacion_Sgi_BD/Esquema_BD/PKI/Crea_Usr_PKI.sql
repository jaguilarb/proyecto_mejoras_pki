CREATE TABLESPACE SEGLC_PKI DATAFILE 
 '/u01/app/oracle/oradata/STPREGPKI/SEGLC_PKI.DBF' SIZE 716800K AUTOEXTEND ON NEXT 10240K MAXSIZE 33554416K
    EXTENT MANAGEMENT LOCAL
ONLINE
FLASHBACK ON
/
CREATE TABLESPACE SEGLI_PKI DATAFILE 
 '/u01/app/oracle/oradata/STPREGPKI/SEGLI_PKI.DBF' SIZE 716800K AUTOEXTEND ON NEXT 10240K MAXSIZE 33554416K
    EXTENT MANAGEMENT LOCAL
ONLINE
FLASHBACK ON
/
CREATE TABLESPACE SEGLT_PKI DATAFILE 
  '/u01/app/oracle/oradata/STPREGPKI/SEGLT_PKI.DBF' SIZE 716800K AUTOEXTEND ON NEXT 10240K MAXSIZE 33554416K
    EXTENT MANAGEMENT LOCAL
ONLINE
FLASHBACK ON
/

CREATE USER SEG_PKI
  IDENTIFIED BY pki
  DEFAULT TABLESPACE USERS
  TEMPORARY TABLESPACE TEMP
  PROFILE DEFAULT
  ACCOUNT UNLOCK
/
  -- 2 Roles for SEG_PKI 
  GRANT CONNECT TO SEG_PKI
/
  GRANT RESOURCE TO SEG_PKI
/
  ALTER USER SEG_PKI DEFAULT ROLE ALL
/
  -- 1 System Privilege for SEG_PKI 
  GRANT UNLIMITED TABLESPACE TO SEG_PKI
/
  -- 3 Tablespace Quotas for SEG_PKI 
  ALTER USER SEG_PKI QUOTA UNLIMITED ON SEGLC_PKI
/
  ALTER USER SEG_PKI QUOTA UNLIMITED ON SEGLI_PKI
/
  ALTER USER SEG_PKI QUOTA UNLIMITED ON SEGLT_PKI
/
